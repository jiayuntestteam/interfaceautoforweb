# coding: utf-8

import os
import unittest
import requests
import configparser as config
import time
import sys
import db_fixture.mysql_db as DB
import db_fixture.test_data as Data
sys.path.append("..")
"""引入配置"""
parentDir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentDir)
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"
cf = config.ConfigParser()
cf.read(file_path)
"""构造测试数据"""
generate_data = Data.init_data("purchase_order")
po_ids = DB.DB().get_field_value("select id from purchase_order order by id desc limit 2", "id")
batch_confirm_id = DB.DB().get_field_value("select purchase_order.id as id, purchase_order.qty_purchased as qty from purchase_order, supply_order where purchase_order.supply_order_id=supply_order.id and purchase_order.qty_purchased >0 and purchase_order.qty_purchased<5 and supply_order.external_order_id is not null and purchase_order.state=0 limit 1", "id")
qty = DB.DB().get_field_value("select purchase_order.id as id, purchase_order.qty_purchased as qty from purchase_order, supply_order where purchase_order.supply_order_id=supply_order.id and purchase_order.qty_purchased >0 and purchase_order.qty_purchased<5 and supply_order.external_order_id is not null and purchase_order.state=0 limit 1", "qty")
#采购批次#
po_ids = DB.DB().get_field_value("select id from purchase_order order by id desc limit 4", "id")
# sku_id = 157477
# item_no = "BAG0000015456"
# supplier = "金湖智盛服装店"
# supplier_url = "www.baidu.com"
# purchaser = "开发者"
# source = "人工"
# is_one_time_supplier = "否"


class PurchaseOrderMgm(unittest.TestCase):
    """ 采购管理"""
    @classmethod
    def setUpClass(cls):
        """ 加载参数配置"""
        cls.base_url = cf.get("Envconf", "pobaseurl")
        cls.content_type = cf.get("Envconf", "Content-Type")
        cls.token = cf.get("Envconf", "token")
        cls.cookies = cf.get("PO", "cookies")
        cls._headers = {
            "Content-Type": cls.content_type,
            "Authorization": cls.token
        }
        cls._cookies = {
            "warehouse": cls.cookies
        }

    def test_get_undo_po_list(self):
        """ 采购 - 查看未采购列表"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)
        print(self.result["results"][0])

    def test_get_purchased_order_list(self):
        """ 采购 - 查看已采购列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_batch_update_external_order_number(self):
        """ 采购 - 批量确认未采购"""
        global batch_confirm_id
        global qty
        test_url = self.base_url + "/purchase_order/quotation/batch_confirm/"
        body = {
            "row_data": [
                {
                    "id": batch_confirm_id[0],
                    "qty_purchased": qty[0]
                }
            ]
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_batch_add_difficult_order(self):
        """ 采购 - 批量转入疑难"""
        global po_ids
        test_url = self.base_url + "/purchase_order/quotation/batch_add_difficult_order/"
        body = {
            "data": [po_ids[0], po_ids[1]]
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_add_purchase_order(self):
        """ 采购 - 新建采购单"""
        test_url = self.base_url + "/purchase_order/quotation/add/"
        body = {
            "sku_id": 2526835,
            "quantity": 1,
            "source_type": 1
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_by_sku_no(self):
        """ 采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "ordering": "-purchase_company_name",
            "stock_keep_unit__sku_no": "0002526835"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_by_supplier_type(self):
        """ 采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "ordering": "-purchase_company_name",
            "supplier_is_disposable": "否"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)
        print(self.result)

    def test_search_by_source(self):
        """ 采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "ordering": "-purchase_company_name",
            "source_type": 1
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_by_follower(self):
        """ 采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "ordering": "-purchase_company_name",
            "follow_user_name__icontains": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_by_supplier_url(self):
        """ 采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "ordering": "-purchase_company_name",
            "purchase_link__icontains": "www.baidu.com"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_by_supplier(self):
        """ 采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "ordering": "-purchase_company_name",
            "purchase_company_name__icontains": "金湖智盛服装店"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_by_item_no(self):
        """ 采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "ordering": "-purchase_company_name",
            "stock_keep_unit__product__item_no": "BAG0000015456"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_purchased_object_by_sku_no(self):
        """ 已采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 1,
            "ordering": "-purchase_company_name",
            "stock_keep_unit__sku_no": "0002526835"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_purchased_object_by_supplier_type(self):
        """ 已采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 1,
            "ordering": "-purchase_company_name",
            "supplier_is_disposable": "否"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)
        print(self.result)

    def test_search_purchased_object_by_source(self):
        """ 已采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 1,
            "ordering": "-purchase_company_name",
            "source_type": 1
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_purchased_object_by_follower(self):
        """ 已采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 1,
            "ordering": "-purchase_company_name",
            "follow_user_name__icontains": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_purchased_object_by_supplier_url(self):
        """ 已采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 1,
            "ordering": "-purchase_company_name",
            "purchase_link__icontains": "www.baidu.com"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_purchased_object_by_supplier(self):
        """ 已采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 1,
            "ordering": "-purchase_company_name",
            "purchase_company_name__icontains": "金湖智盛服装店"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_purchased_object_by_item_no(self):
        """ 已采购 - 搜索"""
        test_url = self.base_url + "/purchase_order/quotation/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 1,
            "ordering": "-purchase_company_name",
            "stock_keep_unit__product__item_no": "BAG0000015456"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_confirm_external_order_number(self):
        """ 更新第三方订单号"""
        global po_ids
        test_update_url = self.base_url + "/purchase_order/supply_order/update/"+str(po_ids[0])+"/"
        body = {
            "external_order_id": "7894561234567899"
        }
        response = requests.post(url=test_update_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_confirm_purchase_order(self):
        """ 确认采购单"""
        global po_ids
        test_confirm_url = self.base_url + "/purchase_order/state/update/"+str(po_ids[3])+"/"
        body = {
            "action": "regular",
            "next_state": 1,
            "qty_purchased": 1
        }
        response = requests.post(url=test_confirm_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    # 打印采购单
    def test_print(self):
        global po_ids
        test_url = self.base_url + "/purchase_order/warehouse/print_purchase_order/"+str(po_ids[0])+"/"
        response = requests.post(url=test_url, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)

    def test_add_supplier(self):
        """ 采购 - 添加候选供应商"""
        test_url = self.base_url + "/purchase_order/link_rel/create/"
        home_url = "https://123.1688."+time.strftime('%Y%m%d%H%M%S', time.localtime(time.time()))+"com/"
        warehouse_location = "北京/东城区"+time.strftime('%H-%M-%S', time.localtime(time.time()))
        product_url = time.strftime('%Y%m%d%H%M%S', time.localtime(time.time()))
        body = {
            "sku_id": 2526835,
            "platform": "1688",
            "home_url": home_url,
            "shop_id": "123",
            "company_type": "1",
            "company_name": "1",
            "warehouse_location": warehouse_location,
            "product_url": product_url,
            "price": "1",
            "link_note": "1"
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_system_demand_po_list(self):
        """ 采购 - 查看系统需求列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "difficult_order": 0,
            "ordering": "-purchase_company_name",
            "source_type": 0
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_manual_demand_po_list(self):
        """ 采购 - 查看人工需求列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "difficult_order": 0,
            "ordering": "-purchase_company_name",
            "source_type": 1
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_additional_demand_po_list(self):
        """ 采购 - 查看补单列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "difficult_order": 0,
            "ordering": "-purchase_company_name",
            "source_type": 2
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_difficult_po_list(self):
        """ 采购 - 查看疑难单列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 0,
            "difficult_order": 1,
            "ordering": "-purchase_company_name"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_out_of_stock_difficult_po_list(self):
        """ 采购 - 查看疑难缺货单列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "state": 6,
            "difficult_order": 1,
            "ordering": "-purchase_company_name"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_no_tracking_no_po_list(self):
        """ 采购 - 查看无运单单列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "logistics_status": "not_shipped"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_uncollected_po_list(self):
        """ 采购 - 查看未揽收单列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "logistics_status": "uncollected"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_partial_collected_po_list(self):
        """ 采购 - 查看部分揽收单列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "logistics_status": "partial_collected"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_collected_po_list(self):
        """ 采购 - 查看全部揽收单列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "logistics_status": "collected"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_unknown_po_list(self):
        """ 采购 - 查看无快递单列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "logistics_status": "unknown"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_have_signed_po_list(self):
        """ 采购 - 查看已到库列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "logistics_status": "have_signed"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_finished_po_list(self):
        """ 采购 - 查看已完成列表"""
        test_url = self.base_url + "/purchase_order/complete/list/"
        payload = {
            "offset": 0,
            "limit": 1,
            "ordering": ""
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_defective_po_list(self):
        """ 采购 - 查看缺发列表"""
        test_url = self.base_url + "/purchase_order/purchased/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "defective_supply_order": "true"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    @classmethod
    def tearDownClass(cls):
        global po_ids
        sql = "delete from purchase_order where id in(%s,%s)" % (po_ids[0],po_ids[1])
        DB.DB().delete_data(sql)


if __name__ == '__main__':
    unittest.main()
