# coding: utf-8

import os
import unittest
import requests
import configparser as config
import time
import sys
import db_fixture.mysql_db as DB
import db_fixture.test_data as Data
sys.path.append("..")
"""引入配置"""
parentDir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentDir)
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"
cf = config.ConfigParser()
cf.read(file_path)
"""构造测试数据"""
generate_data = Data.init_data("goods_return")
return_id = DB.DB().get_field_value("select id from goods_return where application_id='RT0010'", "id")
in_return_id = DB.DB().get_field_value("select id from goods_return where application_id='RT0001'", "id")
back_to_fill_return_id = DB.DB().get_field_value("select id from goods_return where application_id='RT0022'", "id")


class PurchaseOrderDistribution(unittest.TestCase):
    """ 供应商退货"""
    @classmethod
    def setUpClass(cls):
        """ 加载参数配置"""
        cls.base_url = cf.get("Envconf", "pobaseurl")
        cls.content_type = cf.get("Envconf", "Content-Type")
        cls.token = cf.get("Envconf", "token")
        cls.cookies = cf.get("PO", "cookies")
        cls._headers = {
            "Content-Type": cls.content_type,
            "Authorization": cls.token
        }
        cls._cookies = {
            "warehouse": cls.cookies
        }

    def test_get_pending_deal(self):
        """ 待处理页面"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "-purchase_order__uuid",
            "status": 0
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal(self):
        """ 退货ID检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "application_id__exact": "RT0000"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal_by_uuid(self):
        """ 退货UUID检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "purchase_order__uuid": "1804020836006512543338"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal_by_uuid(self):
        """ 创建时间检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "create_time: ": "2018-05-07 06:48:40"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal_by_sku_id(self):
        """ sku检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "sku_no: ": "00035075"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal_by_sku_id(self):
        """ 供应商检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "sku_no: ": "00035075"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal_by_supplier(self):
        """ 供应商url检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "purchase_company_name: ": "深圳市达荣星硅橡胶制品有限公司"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal_by_purchase_link(self):
        """ 供应商url检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "purchase_link: ": "http://detail.1688.com/offer/38222979140.html"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal_by_purchase_link(self):
        """ 供应商url检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "purchase_link: ": "http://detail.1688.com/offer/38222979140.html"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal_by_purchaser(self):
        """ 搜索采购负责人"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "follow_user_name: ": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_deal_by_reurner(self):
        """ 搜索退货负责人"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "applicant_name: ": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_confirm_return_pending_deal(self):
        """" 确认退货"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "-purchase_order__uuid",
            "status": 0,
            "application_id": "RT0000"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_pending_fill_in_address(self):
        """ 填地址页面"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "-purchase_order__uuid",
            "status": 10
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_pending_fill_in_address(self):
        """ 退货ID检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 10,
            "application_id__exact": "RT0010"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_batch_fill_in_address(self):
        """ 批量填写退货地址"""
        global return_id
        test_url = self.base_url + "/return_application/batch_fill_address/"
        body = {
            "return_address": "address",
            "return_consignee_name": "tester",
            "return_consignee_phone": "123456",
            "return_ids": [return_id],
            "return_province": "北京市",
            "send_date": "2018-05-09",
            "status": 1
        }
        response = requests.post(url=test_url, headers=self._headers, cookies=self._cookies, json=body)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_confirm_return_fill_in_address(self):
        """  确认退货"""
        global return_id
        test_url = self.base_url + "/return_application/fill_address/"+str(return_id[0])+"/"
        body = {
            "return_address": "address",
            "return_consignee_name": "tester",
            "return_consignee_phone": "123456",
            "return_province": "北京市",
            "send_date": "2018-05-09",
            "status": 1
        }
        response = requests.post(url=test_url, headers=self._headers, cookies=self._cookies, json=body)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_confirm_return_fill_in_address(self):
        """ 退货中页面"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal(self):
        """ 退货ID检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "application_id__exact": "RT0000"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal_by_uuid(self):
        """ 退货UUID检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "purchase_order__uuid": "1804020836006512543338"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal_by_uuid(self):
        """ 创建时间检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "create_time: ": "2018-05-07 06:48:40"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal_by_sku_id(self):
        """ sku检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "sku_no: ": "00035075"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal_by_sku_id(self):
        """ 供应商检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "sku_no: ": "00035075"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal_by_supplier(self):
        """ 供应商url检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "purchase_company_name: ": "深圳市达荣星硅橡胶制品有限公司"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal_by_purchase_link(self):
        """ 供应商url检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "purchase_link: ": "http://detail.1688.com/offer/38222979140.html"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal_by_purchase_link(self):
        """ 供应商url检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 0,
            "purchase_link: ": "http://detail.1688.com/offer/38222979140.html"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal_by_purchaser(self):
        """ 搜索采购负责人"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "follow_user_name: ": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_returnning_deal_by_reurner(self):
        """ 搜索退货负责人"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "applicant_name: ": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal(self):
        """ 退货ID检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "application_id__exact": "RT0000"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal_by_uuid(self):
        """ 退货UUID检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "purchase_order__uuid": "1804020836006512543338"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal_by_uuid(self):
        """ 创建时间检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "create_time: ": "2018-05-07 06:48:40"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal_by_sku_id(self):
        """ sku检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "sku_no: ": "00035075"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal_by_sku_id(self):
        """ 供应商检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "sku_no: ": "00035075"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal_by_supplier(self):
        """ 供应商url检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "purchase_company_name: ": "深圳市达荣星硅橡胶制品有限公司"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal_by_purchase_link(self):
        """ 供应商url检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "purchase_link: ": "http://detail.1688.com/offer/38222979140.html"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal_by_purchase_link(self):
        """ 供应商url检索"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "purchase_link: ": "http://detail.1688.com/offer/38222979140.html"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal_by_purchaser(self):
        """ 搜索采购负责人"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "follow_user_name: ": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_deal_by_reurner(self):
        """ 搜索退货负责人"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": "2,21,22,23",
            "applicant_name: ": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_batch_fill_in_tracking_no(self):
        """ 批量填物流单号"""
        global in_return_id
        test_url = self.base_url + "/return_application/batch_update_shipping_ticket_no/"
        body = {
            "carrier_name": "顺丰",
            "freight_total_rmb": 5,
            "return_applications": in_return_id,
            "shipping_ticket_no": "123",
            "weight_kg": 0.01
        }
        response = requests.post(url=test_url, headers=self._headers, cookies=self._cookies, json=body)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_by_delivery_date(self):
        """ 发货日期筛选"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "status": 1,
            "send_date__gte": "2018-02-05T00:00:00",
            "send_date__lte": "2018-05-08T23:59:59.999999"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_returned_goods(self):
        """ 已退货页面"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "-purchase_order__uuid",
            "status": "2,21,22,23"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_no_tracking_no_returned_goods(self):
        """ 已退货页面"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "-purchase_order__uuid",
            "status": 21
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_need_tracking_no_returned_good(self):
        """ 无需物流页面"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "-purchase_order__uuid",
            "status": 22
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_need_tracking_no_returned_good(self):
        """ 已回填页面"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "-purchase_order__uuid",
            "status": 23
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_need_tracking_no_returned_good(self):
        """ 已取消"""
        test_url = self.base_url + "/return_application/list/"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "-purchase_order__uuid",
            "status": 3
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_need_tracking_no_returned_good(self):
        """ 批量確認物流回填"""
        global back_to_fill_return_id
        test_url = self.base_url + "/return_application/batch_update_fill_delivery/"
        body = {
            "id_list": [back_to_fill_return_id]
        }
        response = requests.post(url=test_url, headers=self._headers, cookies=self._cookies, json=body)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    @classmethod
    def tearDownClass(cls):
        sql = "delete from goods_return where application_id in(%s,%s,%s,%s,%s,%s,%s,%s,%s)" % ("'RT0000'", "'RT0010'", "'RT0001'", "'RT0002'", "'RT0021'", "'RT0022'", "'RT0023'", "'RT0003'", "'RT0006'")
        DB.DB().delete_data(sql)


if __name__ == '__main__':
    unittest.main()
