# coding: utf-8

import os
import unittest
import requests
import configparser as config
import time
import sys
import db_fixture.mysql_db as DB
import db_fixture.test_data as Data
sys.path.append("..")
"""引入配置"""
parentDir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentDir)
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"
cf = config.ConfigParser()
cf.read(file_path)
"""构造测试数据"""
generate_data = Data.init_data("purchase_order")
uu_ids = DB.DB().get_field_value("select uuid from purchase_order where purchase_order.state=0 limit 4", "uuid")


class PurchaseOrderDistribution(unittest.TestCase):
    """ 采购单分配"""
    @classmethod
    def setUpClass(cls):
        """ 加载参数配置"""
        cls.base_url = cf.get("Envconf", "pobaseurl")
        cls.content_type = cf.get("Envconf", "Content-Type")
        cls.token = cf.get("Envconf", "token")
        cls.cookies = cf.get("PO", "cookies")
        cls._headers = {
            "Content-Type": cls.content_type,
            "Authorization": cls.token
        }
        cls._cookies = {
            "warehouse": cls.cookies
        }

    def test_get_undo_po_list(self):
        """ 采购 - 查看分单页列表"""
        test_url = self.base_url + "/purchase_order/purchase_assign/"
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_calculate_distributed_sys_po_num(self):
        """ 计算已分配系统单数量 """
        test_url = self.base_url + "/purchase_order/quotation/count/"
        payload = {
            "source_types": 0,
            "difficult_order": 0,
            "follow_users": "not null",
            "assign_type": "purchaser"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_calculate_distributed_manual_po_num(self):
        """ 计算已分配人工单数量 """
        test_url = self.base_url + "/purchase_order/quotation/count/"
        payload = {
            "source_types": 1,
            "difficult_order": 0,
            "follow_users": "not null",
            "assign_type": "purchaser"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_calculate_additional_po_num(self):
        """ 计算已分配补单数量 """
        test_url = self.base_url + "/purchase_order/quotation/count/"
        payload = {
            "source_types": 2,
            "difficult_order": 0,
            "follow_users": "not null",
            "assign_type": "purchaser"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_calculate_po_num_by_category(self):
        """ 计算商品品类数量 """
        test_url = self.base_url + "/purchase_order/quotation/count/"
        payload = {
            "category_names": "womens_clothing",
            "difficult_order": 0,
            "follow_users": "not null",
            "assign_type": "purchaser"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_calculate_po_num_by_uuid(self):
        """ 计算采购区间数量"""
        global uu_ids
        test_url = self.base_url + "/purchase_order/quotation/count/"
        payload = {
            "uuids__gte": uu_ids[0][0:12],
            "uuids__lt": uu_ids[1][0:12],
            "difficult_order": 0,
            "follow_users": "not null",
            "assign_type": "purchaser"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_calculate_po_num_by_supplier(self):
        """ 计算供应商数量"""
        test_url = self.base_url + "/purchase_order/quotation/count/"
        payload = {
            "purchase_company_names": "义乌中茂电子商务有限公司",
            "difficult_order": 0,
            "follow_users": "not null",
            "assign_type": "purchaser"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_calculate_distributed_po_num(self):
        """ 计算已分配数量"""
        test_url = self.base_url + "/purchase_order/quotation/count/"
        payload = {
            "difficult_order": 0,
            "follow_users": "not null",
            "assign_type": "purchaser"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_calculate_un_distribute_po_num(self):
        """ 计算未分配数量"""
        test_url = self.base_url + "/purchase_order/quotation/count/"
        payload = {
            "difficult_order": 0,
            "follow_users": "null",
            "assign_type": "purchaser"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_distribute_person_in_charge(self):
        """ 分配负责人"""
        global uu_ids
        test_url = self.base_url + "/purchase_order/quotation/assign/"
        payload = {
            "uuids__gte": uu_ids[0][0:12],
            "uuids__lt": uu_ids[1][0:12],
            "difficult_order": 0,
            "follow_users": "not null",
            "assign_type": "purchaser"
        }
        body = {
            "to_users": [{"id": "113", "last_name": "tester"}],
            "assign_per_person": "1"

        }
        response = requests.post(url=test_url, headers=self._headers, cookies=self._cookies, params=payload,json=body)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_distribute_difficult_person_in_charge(self):
        """ 分配疑难负责人"""
        global uu_ids
        test_url = self.base_url + "/purchase_order/quotation/assign/"
        payload = {
            "uuids__gte": uu_ids[0][0:12],
            "uuids__lt": uu_ids[0][0:12],
            "difficult_order": 1,
            "follow_users": "not null",
            "assign_type": "purchaser"
        }
        body = {
            "to_users": [{"id": "113", "last_name": "tester"}],
            "assign_per_person": "1"

        }
        response = requests.post(url=test_url, headers=self._headers, cookies=self._cookies, params=payload,json=body)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    @classmethod
    def tearDownClass(cls):
        print()


if __name__ == '__main__':
    unittest.main()
