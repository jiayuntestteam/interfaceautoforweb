# coding: utf-8

import os
import unittest
import requests
import configparser as config
import sys
import db_fixture.mysql_db as DB
import db_fixture.test_data as Data
sys.path.append("..")
"""引入配置"""
parentDir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentDir)
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"
cf = config.ConfigParser()
cf.read(file_path)
"""构造测试数据"""
generate_data = Data.init_data("purchase_order")
po_ids = DB.DB().get_field_value("select id from purchase_order order by id desc limit 2", "id")
uuid = DB.DB().get_field_value("select uuid from purchase_order order by id desc limit 1", "uuid")
task_id = DB.DB().get_field_value("SELECT signed_task.id as id FROM shipping_ticket,signed_task WHERE shipping_ticket.task_id = signed_task.id AND task_id != 0 AND signed_status = 0 AND signed_task.end_time IS NULL limit 1","id")
print(task_id)


class StockInTest(unittest.TestCase):
    """ 入库"""
    @classmethod
    def setUpClass(cls):
        """ 加载参数配置"""
        cls.base_url = cf.get("Envconf", "pobaseurl")
        cls.content_type = cf.get("Envconf", "Content-Type")
        cls.token = cf.get("Envconf", "token")
        cls.cookies = cf.get("PO", "cookies")
        cls._headers = {
            "Content-Type": cls.content_type,
            "Authorization": cls.token
        }
        cls._cookies = {
            "warehouse": cls.cookies
        }

    def test_get_undo_stock_in_order(self):
        """入库 - 查看未完成入库单"""
        test_url = self.base_url + "/purchase_order/warehouse/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C3",
            "ordering": ""
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_stock_in_order(self):
        """入库 - 查看已完成入库单"""
        payload = {
            "offset": 0,
            "limit": 1,
            "ordering": ""
        }
        test_url = self.base_url + "/purchase_order/complete/list/"
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_get_signed_task(self):
        """入库 - 查看已签收入库单"""
        test_url = self.base_url + "/purchase_order/package/signed_task/list"
        payload = {
            "offset": 0,
            "limit": 20
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_signed_exception_task(self):
        """入库 - 签收异常单"""
        test_url = self.base_url + "/purchase_order/package/task_detail/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "signed_status__gt": 0,
            "ordering":""
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_batch_check(self):
        """ 入库 - 质检调整 - 检查"""
        global po_ids
        test_url = self.base_url + "/purchase_order/info/batch_check/"
        body = {
            "quality_type": "NORMAL",
            "uuid_list": [str(po_ids[0]), str(po_ids[1])]
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        print(self.result)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['code'], 0)
        self.assertLessEqual(response.elapsed.total_seconds(), 5)

    def test_batch_update(self):
        """ 入库 - 质检调整 - 更新"""
        global po_ids
        test_url = self.base_url + "/purchase_order/info/batch_update/"
        body = {
            "quality_type": "NORMAL",
            "uuid_list": [str(po_ids[0]), str(po_ids[1])]
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        print(self.result)
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['code'], 0)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_add_new_po_batch(self):
        """ 入库 - 新建采购批次"""
        test_url = self.base_url + "/purchase_order/quotation/add/"
        body = {
            "sku_id": 2526835,
            "quantity": 1,
            "source_type": 1
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_batch_add_tracking_no(self):
        """入库 - 批量新增物流单号"""
        test_url = self.base_url + "/purchase_order/quotation/add/"
        body = {
            "sku_id": 2526835,
            "quantity": 1,
            "source_type": 1
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_by_po_batch_number(self):
        """入库 - 已完成 - 检索采购批次"""
        test_url = self.base_url + "/purchase_order/complete/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "uuid__icontains": "1804020653068444819628"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_new_sign_task(self):
        """入库 - 签收 - 新建签收任务"""
        test_url = self.base_url + "/purchase_order/package/signed_task/create"
        payload = {
            "task_name": "test12127"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_by_logistics_number(self):
        """入库 - 签收 - 物流单号检索"""
        test_url = self.base_url + "/purchase_order/signed/search/tracking_no/"
        body = {
            "task_id": 31,
            "tracking_no": 'test12127'
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_complete_sign_task(self):
        """入库 - 签收 - 完成本次签收 """
        global task_id
        test_url = self.base_url + "/purchase_order/package/signed_task/update/"
        body = {
            "task_id": task_id[0]
        }
        response = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['is_success'], True)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_sign_exception_search_by_date(self):
        """签收异常 - 签收日期搜索"""
        test_url = self.base_url + "/purchase_order/package/task_detail/list/"
        payload = {
            "signed_status__gt": 0,
            "signed_date__gte": '2018-03-01',
            "signed_date__lte": '2018-04-08',
            "offset": 0,
            "limit": 20,
            "signed_date__gte": '2018-03-01',
            "signed_date__lte": "2018-04-08",
            "&signed_status__gt": 0,
            "&ordering=": ""
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies,params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_unfinished_by_logistics_number(self):
        """未完成 - 物流单号检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C31%2C2%2C3",
            "ordering": "",
            "tracking_no": "test456123456789"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_unfinished_by_uuid(self):
        """未完成 - 采购单号检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C31%2C2%2C3",
            "ordering": "",
            "uuid__icontains": uuid[0]
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_unfinished_by_uuid(self):
        """未完成 - 货号检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C31%2C2%2C3",
            "ordering": "",
            "stock_keep_unit__product__item_no": "BAG0000015456"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_unfinished_by_supplier(self):
        """未完成 - 供应商检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C31%2C2%2C3",
            "ordering": "",
            "purchase_company_name__icontains": "金湖智盛服装店"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_unfinished_by_supplier_url(self):
        """未完成 - 供应商url检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C31%2C2%2C3",
            "ordering": "",
            "purchase_link__icontains": "www.baidu.com"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_unfinished_by_sku_number(self):
        """未完成 - sku检索"""
        test_url = self.base_url + "/purchase_order/warehouse/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C3",
            "ordering": "",
            "stock_keep_unit__sku_no": "0004259847"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_unfinished_by_follow_user(self):
        """未完成 - 采购负责人检索"""
        test_url = self.base_url + "/purchase_order/warehouse/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C3",
            "ordering": "",
            "follow_user_name__icontains": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_unfinished_by_source_type(self):
        """未完成 - 来源类型检索"""
        test_url = self.base_url + "/purchase_order/warehouse/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C3",
            "ordering": "",
            "source_type": 1
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_unfinished_by_is_one_time_supplier(self):
        """未完成 - 是否一次性供应商检索"""
        test_url = self.base_url + "/purchase_order/warehouse/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "state__in": "1%2C2%2C3",
            "ordering": "",
            "supplier_is_disposable": 0
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_by_logistics_number(self):
        """未完成 - 物流单号检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "tracking_no": "test456123456789"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_by_uuid(self):
        """未完成 - 采购单号检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "uuid__icontains": uuid[0]
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_by_uuid(self):
        """未完成 - 货号检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "stock_keep_unit__product__item_no": "BAG0000015456"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_by_supplier(self):
        """未完成 - 供应商检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "purchase_company_name__icontains": "金湖智盛服装店"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_by_supplier_url(self):
        """未完成 - 供应商url检索"""
        test_url = self.base_url + "/purchase_order/warehouse/tracking_no/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "purchase_link__icontains": "www.baidu.com"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_by_sku_number(self):
        """未完成 - sku检索"""
        test_url = self.base_url + "/purchase_order/warehouse/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "stock_keep_unit__sku_no": "0004259847"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_by_follow_user(self):
        """未完成 - 采购负责人检索"""
        test_url = self.base_url + "/purchase_order/warehouse/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "follow_user_name__icontains": "开发者"
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_by_source_type(self):
        """未完成 - 来源类型检索"""
        test_url = self.base_url + "/purchase_order/warehouse/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "source_type": 1
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    def test_search_finished_by_is_one_time_supplier(self):
        """未完成 - 是否一次性供应商检索"""
        test_url = self.base_url + "/purchase_order/warehouse/list"
        payload = {
            "offset": 0,
            "limit": 20,
            "ordering": "",
            "supplier_is_disposable": 0
        }
        response = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=payload)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), 2)

    @classmethod
    def tearDownClass(cls):
        global po_ids
        sql1 = "delete from purchase_order where id in(%s,%s)" % (po_ids[0], po_ids[1])
        DB.DB().delete_data(sql1)
        sql2 = "update signed_task set end_time=null where id=%s"%task_id[0]
        DB.DB().update(sql2)


if __name__ == '__main__':
    unittest.main()
