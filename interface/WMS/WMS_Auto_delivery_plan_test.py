# coding: utf-8

# WMS给前端，物流通道自动选择

import os
import sys
import unittest
import requests
import configparser as cparser
import db_fixture.mysql_db as my_DB
from datetime import timedelta, date
from db_fixture import test_data

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)


class WMSAutoDeliveryPlanTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.base_url = cf.get("Envconf", "WMSBaseUrl")
        cls.content_type = cf.get("Envconf", "Content-Type")
        cls.token = cf.get("Envconf", "token")
        cls._headers = {
            "Content-Type": cls.content_type,
            "Authorization": cls.token
        }
        cls.response_time_limit = cf.get("Envconf", "response_time_limit")
        cls.countries = ["india"]

    @classmethod
    def tearDownClass(cls):
        print("End of the test WMSAutoDeliveryPlanTest")

        # def tearDown(self):
        #     print(self.result)

    def test_get_daily_logistics_plan(self):
        """物流通道 - 获取物流计划表"""
        test_url = self.base_url + "/wms/daily_logistics_plan"
        payload = {
            "country": self.countries[0],
            "start_date": str(date.today())
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_create_daily_logistics_plan_fail(self):
        """物流通道 - 批量创建物流计划表 - 创建当天"""
        test_url = self.base_url + "/wms/daily_logistics_plan"
        data = {
            "plans": [
                {
                    "plan_date": str(date.today()),
                    "country": self.countries[0],
                    "quota": {
                        "del": 3000,
                        "bom": 1000
                    }
                }
            ]
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["message"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_create_daily_logistics_plan_success(self):
        """物流通道 - 批量创建物流计划表 - 创建之后两天"""
        test_url = self.base_url + "/wms/daily_logistics_plan"
        data = {
            "plans": [
                {
                    "plan_date": str(date.today() + timedelta(days=1)),
                    "country": self.countries[0],
                    "quota": {
                        "del": 3000,
                        "bom": 1000
                    }
                },
                {
                    "plan_date": str(date.today() + timedelta(days=2)),
                    "country": self.countries[0],
                    "quota": {
                        "del": 3000,
                        "bom": 1000
                    }
                }
            ]
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


if __name__ == '__main__':
    test_data.init_data()  # 初始化接口测试数据
    unittest.main()
