# coding: utf-8

# WMS给前端，库位管理

import os
import sys
import unittest
import requests
import configparser as cparser
import db_fixture.mysql_db as my_DB

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)

# ======== Global Paras ===============
# retrieve_test_data = [{}]

class WMSBoxManagementTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.base_url = cf.get("Envconf", "WMSBaseUrl")
        self.content_type = cf.get("Envconf", "Content-Type")
        self.token = cf.get("Envconf", "token")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }
        self.response_time_limit = cf.get("Envconf", "response_time_limit")
        self.box_count = 3
        self.sql_select_order_box = "select * from box_order WHERE stock_picking_id = 0 order by id DESC limit %d" % self.box_count
        self.sql_select_invalid_order_box = "select * from box_order WHERE stock_picking_id != 0 order by id DESC limit %d" % self.box_count
        self.sql_select_idle_box = "select * from box_sku WHERE status = 'FREE' order by id DESC limit %d" % self.box_count
        self.sql_select_invalid_idle_box = "select * from box_sku WHERE status != 'FREE' order by id DESC limit %d" % self.box_count
        self.test_DB = my_DB.DB()
        self.order_box_names = self.test_DB.get_field_value(self.sql_select_order_box, "name")
        self.invalid_order_box_names = self.test_DB.get_field_value(self.sql_select_invalid_order_box, "name")
        self.idle_box_names = self.test_DB.get_field_value(self.sql_select_idle_box, "name")
        self.invalid_idle_box_names = self.test_DB.get_field_value(self.sql_select_invalid_idle_box, "name")

    @classmethod
    def tearDownClass(self):
        print("End of the test WMSBoxManagementTest")

    def tearDown(self):
        print(self.result)

    def test_check_order_box_delete(self):
        """库位管理 - 增减库位 - 检查订单库位（删除）"""
        test_url = self.base_url + "/wms/box_order_check"
        data = {
            "type": "delete",
            "box_name": self.order_box_names
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["meets"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_check_order_box_insert(self):
        """库位管理 - 增减库位 - 检查订单库位（新增）"""
        test_url = self.base_url + "/wms/box_order_check"
        data = {
            "type": "insert",
            "box_name": self.order_box_names,
            "box_norm": u"单层5分位"
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["meets"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_del_order_box(self):
        """库位管理 - 增减库位 - 删除订单库位"""
        test_url = self.base_url + "/wms/box_order"
        payload = {
            "box_name": ','.join(self.order_box_names)
        }
        r = requests.delete(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["success"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_z_add_order_box(self):
        """库位管理 - 增减库位 - 新增订单库位"""
        test_url = self.base_url + "/wms/box_order"
        data = {
            "box_name": self.order_box_names,
            "box_norm": u"单层5分位"
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["success"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_check_order_box_fail(self):
        """库位管理 - 增减库位 - 检查订单库位"""
        test_url = self.base_url + "/wms/box_order_check"
        data = {
            "type": "delete",
            "box_name": self.invalid_order_box_names,
            "box_norm": u"单层5分位"
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["not_meets"]["count"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_del_order_box_fail(self):
        """库位管理 - 增减库位 - 删除订单库位"""
        test_url = self.base_url + "/wms/box_order"
        payload = {
            "box_name": ','.join(self.invalid_order_box_names)
        }
        r = requests.delete(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["fail"]["count"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_z_add_order_box_fail(self):
        """库位管理 - 增减库位 - 新增订单库位"""
        test_url = self.base_url + "/wms/box_order"
        data = {
            "box_name": self.invalid_order_box_names,
            "box_norm": u"单层5分位"
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["fail"]["count"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_check_idle_box_delete(self):
        """库位管理 - 增减库位 - 检查闲置库位（删除）"""
        test_url = self.base_url + "/wms/box_sku_check"
        data = {
            "type": "delete",
            "box_name": self.idle_box_names,
            "category": "饰品类"
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["meets"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_check_idle_box_insert(self):
        """库位管理 - 增减库位 - 检查闲置库位（新增）"""
        test_url = self.base_url + "/wms/box_sku_check"
        data = {
            "type": "insert",
            "box_name": self.idle_box_names,
            "category": "饰品类"
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["meets"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_del_idle_box(self):
        """库位管理 - 增减库位 - 删除闲置库位"""
        test_url = self.base_url + "/wms/box_sku"
        payload = {
            "box_name": ','.join(self.idle_box_names)
        }
        r = requests.delete(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["success"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_z_add_idle_box(self):
        """库位管理 - 增减库位 - 新增闲置库位"""
        test_url = self.base_url + "/wms/box_sku"
        data = {
            "category": "饰品类",
            "upper_limit": 15,
            "box_name": self.order_box_names
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["success"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_check_idle_box_fail(self):
        """库位管理 - 增减库位 - 检查闲置库位"""
        test_url = self.base_url + "/wms/box_sku_check"
        data = {
            "type": "delete",
            "box_name": self.invalid_idle_box_names,
            "category": "饰品类"
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["not_meets"]["count"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_del_idle_box_fail(self):
        """库位管理 - 增减库位 - 删除闲置库位"""
        test_url = self.base_url + "/wms/box_sku"
        payload = {
            "box_name": ','.join(self.invalid_idle_box_names)
        }
        r = requests.delete(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["fail"]["count"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_z_add_idle_box_fail(self):
        """库位管理 - 增减库位 - 新增闲置库位"""
        test_url = self.base_url + "/wms/box_sku"
        data = {
            "category": "饰品类",
            "upper_limit": 15,
            "box_name": self.invalid_idle_box_names
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["fail"]["count"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_y_modify_idle_box(self):
        """库位管理 - 增减库位 - 编辑闲置库位"""
        test_url = self.base_url + "/wms/box_sku"
        data = {
            "type": "edit",
            "category": "鞋包类",
            "upper_limit": 20,
            "box_name": self.invalid_idle_box_names
        }
        r = requests.patch(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["fail"]["count"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_y_modify_order_box(self):
        """库位管理 - 增减库位 - 编辑订单库位"""
        test_url = self.base_url + "/wms/box_sku"
        data = {
            "box_norm": "单层2分位",
            "type": "edit",
            "box_name": self.invalid_order_box_names
        }
        r = requests.patch(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result["fail"]["count"], self.box_count)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_search_box_list(self):
        """库位管理 - 检索库位"""
        test_url = self.base_url + "/wms/box/manage"
        payload = {"box_type": "box_order",
                   "box_norm": "单层5分位",
                   "is_available": "true",
                   "size": 50,
                   "page": 1}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


if __name__ == '__main__':
    test_data.init_data()  # 初始化接口测试数据
    unittest.main()
