# coding: utf-8

# WMS给前端，盘点相关

import os
import sys
import unittest
import requests
import configparser as cparser
import db_fixture.mysql_db as my_DB

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
# from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)

box_name = ""
is_new_task_available = False


class WMSForFETest(unittest.TestCase):

    def setUp(self):
        self.base_url = cf.get("Envconf", "WMSBaseUrl")
        self.content_type = cf.get("Envconf", "Content-Type")
        self.token = cf.get("Envconf", "token")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }
        self.respons_time_limit = cf.get("Envconf", "response_time_limit")
        # self.sku_id = self.getFieldValue("sku_id", 0)
        self.test_DB = my_DB.DB()

    def tearDown(self):
        print(self.result)
        self.test_DB.close()

    @staticmethod
    def getFieldValue(self, filed, sql_index):
        sqls = [
            "SELECT %s FROM `idle_sku` WHERE box_name != '' ORDER BY id DESC LIMIT 1" % filed,
            "SELECT %s FROM `idle_sku` WHERE status = 'IDLE_UNALLOCATED' ORDER BY id DESC LIMIT 1" % filed,
            "SELECT %s FROM `idle_sku` WHERE status = 'IDLE_TO_OFF_SHELF' ORDER BY id DESC LIMIT 1" % filed
        ]
        try:
            filed_value = self.test_DB.get_field_value(sqls[sql_index], filed)
        except Exception as e:
            print e
            return [""]
        return filed_value[0]

    def test_search_with_sku(self):
        '''出库单 - 检索'''
        test_url = self.base_url + "/wms/stock_picking_line"
        payload = {"sku_id": self.getFieldValue(self, "sku_id", 0)}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    # def test_stock_in_for_PO(self):
    #     '''采购 - 移入出库单'''
    #     test_url = self.base_url + "/wms/stock_picking_line/purchase_order"
    #     data = {
    #         "po_id": 224137,
    #         "spl_id": 21922,
    #         "box_name": "E01-1-03",
    #         "sku_id": 23003,
    #         "qty": 1
    #     }

    # def test_stock_in_for_PO(self):
    #     '''采购 - 移入闲置库存'''
    #     test_url = self.base_url + "/wms/stock_picking_line/purchase_order"
    #     data = {
    #         "po_id":224086,
    #         "sku_id":713841,
    #         "sku_category":"饰品类",
    #         "box_name":"X01-1-05",
    #         "qty":1
    #     }

    def test_idle_sku_info_by_sku(self):
        '''库存 - 检索'''
        test_url = self.base_url + "/wms/idle_sku"
        payload = {
            "sku_id": self.sku_id,
            "page": 1,
            "count": 20
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_idle_box_name_by_sku(self):
        '''库存 - SKU所在库位'''
        test_url = self.base_url + "/wms/idle_sku/shelved"
        payload = {
            "sku_id": self.getFieldValue(self, "sku_id", 0),
            "page": 1,
            "count": 20
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_free_idle_sku(self):
        '''库存 - 获取库位'''
        global box_name
        test_url = self.base_url + "/wms/box_sku"
        payload = {
            "query": "free",
            "category": "轻薄服装类"
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        box_name = self.result["name"]
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_category_for_idle_box(self):
        '''库存 - 类型列表'''
        test_url = self.base_url + "/wms/static"
        payload = {
            "query": "category"
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_stock_into_idle_box(self):
        '''库存 - 移入'''
        test_url = self.base_url + "/wms/idle_sku"
        data = {
            "sku_id": self.getFieldValue(self, "sku_id", 1),
            "sku_category": "轻薄服装类",
            "box_name": box_name,
            "qty": 1
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_box_info_by_PO(self):
        '''根据采购批次的ID获取对应的库位记录'''
        test_url = self.base_url + "/wms/stock/purchase_order"
        payload = {
            "po_id": 1
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_judge_new_task(self):
        '''库存 - 能否计算新任务'''
        global is_new_task_available
        test_url = self.base_url + "/wms/idle_sku/plan_off_shelf/can_recalculate"
        r = requests.get(url=test_url, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)
        if self.result["can_recalculate"] == "yes":
            is_new_task_available = True

    def test_calculate_new_task(self):
        '''库存 - 计算新任务[会清空已分配]'''
        test_url = self.base_url + "/wms/idle_sku/plan_off_shelf"
        data = {
            "left": "",
            "right": "",
            "assignee_id": "",
            "new_assignee_id": 1,
            "new_assignee_name": "管理"
        }
        if is_new_task_available:
            r = requests.post(url=test_url, json=data, headers=self._headers)
            self.assertEqual(r.status_code, requests.codes.ok)
            self.result = r.json()
            self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)
        else:
            self.result = ""

    def test_get_unfinished_task(self):
        '''库存 - 查看未完成任务'''
        test_url = self.base_url + "/wms/idle_sku/plan_off_shelf"
        payload = {
            "left": "X01-1-01",
            "right": "X14-1-21",
            "assignee_id": "0,1,2",
            "is_exclude": 1
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_assign_unfinished_task(self):
        '''库存 - 计划闲置移出'''
        test_url = self.base_url + "/wms/idle_sku/plan_off_shelf"
        data = {
            "left": "",
            "right": "",
            "assignee_id": self.getFieldValue(self, "assignee_id", 2),
            "is_exclude": 0,
            "new_assignee_id": 1,
            "new_assignee_name": "管理"
        }
        r = requests.patch(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)


if __name__ == '__main__':
    # test_data.init_data() # 初始化接口测试数据
    unittest.main()