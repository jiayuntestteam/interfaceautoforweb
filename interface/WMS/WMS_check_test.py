# coding: utf-8

# WMS给前端，盘点相关

import os
import sys
import unittest
import requests
import configparser as cparser

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)


class WMSCheckTest(unittest.TestCase):

    def setUp(self):
        self.base_url = cf.get("Envconf", "WMSBaseUrl")
        self.content_type = cf.get("Envconf", "Content-Type")
        self.token = cf.get("Envconf", "token")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }

    def tearDown(self):
        print(self.result)

    def test_check_with_boxname(self):
        '''库存 - 盘点'''
        test_url = self.base_url + "/wms/check"
        payload = {"box_name": "X01-02-14"}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_change_sku_status(self):
        '''库存 - 盘点修改sku状态'''
        test_url = self.base_url + "/wms/check/sku"
        body = {"box_name": "X01-1-02",
                "id": 235,
                "to_status": "IDLE_SHELVED"}
        r = requests.patch(url=test_url, data=body, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_get_status_to_modify(self):
        '''出库单/库存 - 盘点获取可更改的状态选项'''
        test_url = self.base_url + "/wms/static"
        payload = {"query": "status", "box_name": "X01-1-02"}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_get_sku_info(self):
        '''根据sku_id查找sku信息'''
        test_url = self.base_url + "/wms/sku/199670"
        r = requests.get(url=test_url, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)


if __name__ == '__main__':
    test_data.init_data() # 初始化接口测试数据
    unittest.main()