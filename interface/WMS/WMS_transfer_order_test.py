# coding: utf-8

# WMS给前端，物流通道自动选择

import os
import sys
import unittest
import requests
import configparser as cparser
import db_fixture.mysql_db as my_DB
from datetime import timedelta, date
from db_fixture import test_data

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)

# ======== Global Paras ===============
sku_info = []
# TO_name = ""


class WMSTransferOrderTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.base_url = cf.get("Envconf", "WMSBaseUrl")
        cls.content_type = cf.get("Envconf", "Content-Type")
        cls.token = cf.get("Envconf", "token")
        cls._headers = {
            "Content-Type": cls.content_type,
            "Authorization": cls.token
        }
        cls.response_time_limit = cf.get("Envconf", "response_time_limit")
        cls.test_DB = my_DB.DB()

    @classmethod
    def tearDownClass(cls):
        cls.test_DB.close()
        print("End of the test WMSWeighTest")

    def tearDown(self):
        # self.test_DB.close()
        print(self.result)

    def test_get_available_sku_list_for_transfer_order(self):
        """[调拨/退供]出库 - 查询可用sku"""
        test_url = self.base_url + "/wms/idle_sku/transfer_order"
        r = requests.get(test_url, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_available_sku_list_for_transfer_order_with_condition(self):
        """[调拨/退供]出库 - 带条件查询可用sku"""
        global sku_info

        sql_for_available_sku = "SELECT * FROM `idle_sku` WHERE `status` = 'IDLE_SHELVED' ORDER BY id DESC limit 1"
        sku_ids = self.test_DB.get_field_value(sql_for_available_sku, 'sku_id')
        box_names = self.test_DB.get_field_value(sql_for_available_sku, 'box_name')

        test_url = self.base_url + "/wms/idle_sku/transfer_order"
        payload = {
            'count': 50,
            'sku_id': sku_ids[0],
            'from_box': box_names[0],
            'to_box': box_names[0],
            'page': 1
        }
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

        sku_info = self.result

    def test_create_transfer_order(self):
        """[调拨/退供]出库 - 生成出库单"""
        # global TO_name

        sql_for_available_sku = "SELECT * FROM `idle_sku` WHERE `status` = 'IDLE_SHELVED' ORDER BY id DESC limit 1"
        sku_ids = self.test_DB.get_field_value(sql_for_available_sku, 'sku_id')
        box_names = self.test_DB.get_field_value(sql_for_available_sku, 'box_name')

        test_url = self.base_url + "/wms/transfer_order/stock_out"
        data = {
            "destination": "xiaoshan",
            "stock_out_type": "transfer_order",
            "transfer_order_line": [
                {
                    "sku_id": sku_ids[0],
                    "box_name": box_names[0],
                    "transfer_qty": 1
                }
            ]
        }
        r = requests.post(test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        # TO_name = self.result["name"]

    def test_del_transfer_order(self):
        """[调拨/退供]出库 - 取消出库单"""
        sql_for_TOs = "SELECT * FROM `transfer_order` where `status` = 'INIT' ORDER BY id DESC LIMIT 1"
        TO_name = self.test_DB.get_field_value(sql_for_TOs, "name")

        test_url = self.base_url + "/wms/transfer_order/stock_out"
        data = {
            "name": TO_name[0],
            "operation": "cancel"
        }
        r = requests.patch(test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_init_transfer_order_list(self):
        """[调拨/退供]出库 - 检索出库单"""
        test_url = self.base_url + "/wms/stock_warehouse/stock_picking/plan_out"
        payload = {
            "shipping_name": "xiaoshan",
            "assignee_id": "",
            "plan_out_limit": 200
        }
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_assign_transfer_order(self):
        """[调拨/退供]出库 - 分配出库责任人"""
        test_url = self.base_url + "/wms/stock_warehouse/stock_picking/plan_out"
        data = {
            "assignee_id": "",
            "new_assignee_id": 113,
            "new_assignee_name": "tester",
            "plan_out_limit": 200,
            "shipping_name": "xiaoshan"
        }
        r = requests.patch(test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_out_transfer_order(self):
        """[调拨/退供]出库 - 出库核查"""
        sql_for_TOs = "SELECT * FROM `transfer_order` where `status` = 'PLANNED_OUT' ORDER BY id DESC LIMIT 1"
        TO_name = self.test_DB.get_field_value(sql_for_TOs, "name")

        test_url = self.base_url + "/wms/stock_warehouse/out"
        payload = {
            "sp_name": TO_name[0]
        }
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_send_out_transfer_order(self):
        """[调拨/退供]出库 - 出库确认"""
        sql_for_TOs = "SELECT * FROM transfer_order_line WHERE to_name in (SELECT x.name from (SELECT `name` from " \
                      "transfer_order WHERE `status` = 'PLANNED_OUT' ORDER BY id DESC LIMIT 1) as x)"
        TO_name = self.test_DB.get_field_value(sql_for_TOs, "to_name")
        TO_line_ids = self.test_DB.get_field_value(sql_for_TOs, "id")

        test_url = self.base_url + "/wms/stock_warehouse/out"
        data = {
           "sp_name": TO_name[0], "sp_ids": TO_line_ids
        }
        r = requests.patch(test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_transfer_order_stock_in_info(self):
        """调拨入库 - 获取入库单详情"""
        sql_for_TOs = "SELECT * FROM `transfer_order` where `status` = 'ALL_OUT' ORDER BY id DESC LIMIT 1"
        TO_name = self.test_DB.get_field_value(sql_for_TOs, "name")

        test_url = self.base_url + "/wms/transfer_order/stock_in"
        payload = {
            "origin_name": TO_name[0]
        }
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_transfer_order_stock_in_sku_info(self):
        """调拨入库 - 查询sku"""
        sql_for_TOs = "SELECT * FROM transfer_order_line WHERE to_name in (SELECT x.name from (SELECT `name` from " \
                      "transfer_order WHERE `status` = 'ALL_OUT' ORDER BY id DESC LIMIT 1) as x)"
        TO_name = self.test_DB.get_field_value(sql_for_TOs, "to_name")
        TO_line_ids = self.test_DB.get_field_value(sql_for_TOs, "id")

        test_url = self.base_url + "/wms/transfer_order/stock_in_sku"
        payload = {
            "origin_name": TO_name[0],
            "sku_id": TO_line_ids[0]
        }
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_update_transfer_order_stock_in(self):
        """调拨入库 - 更新sku签收数量"""
        sql_for_TOs = "SELECT * FROM transfer_order_line WHERE to_name in (SELECT x.name from (SELECT `name` from " \
                      "transfer_order WHERE `status` = 'ALL_OUT' ORDER BY id DESC LIMIT 1) as x)"
        TO_name = self.test_DB.get_field_value(sql_for_TOs, "to_name")
        TO_line_ids = self.test_DB.get_field_value(sql_for_TOs, "id")

        test_url = self.base_url + "/wms/transfer_order/stock_in_sku"
        data = {
            "origin_name": TO_name[0],
            "sku_id": TO_line_ids[0],
            "qty": 1
        }
        r = requests.patch(test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_update_transfer_order_stock_in(self):
        """调拨入库 - 更新sku签收数量"""
        sql_for_TOs = "SELECT * FROM `transfer_order` where `status` = 'ALL_OUT' ORDER BY id DESC LIMIT 1"
        TO_name = self.test_DB.get_field_value(sql_for_TOs, "name")

        test_url = self.base_url + "/wms/transfer_order/all_stock_in"
        data = {
            "origin_name": TO_name[0]
        }
        r = requests.patch(test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


if __name__ == '__main__':
    test_data.init_data()  # 初始化接口测试数据
    unittest.main()
