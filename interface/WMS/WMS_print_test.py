# coding: utf-8

# WMS给前端，打印相关

import os
import sys
import unittest
import requests
import configparser as cparser
sys.path.append("..")
import db_fixture.test_data as generate_data

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)


class WMSPrintTest(unittest.TestCase):

    def setUp(self):
        self.base_url = cf.get("Envconf", "WMSBaseUrl")
        self.content_type = cf.get("Envconf", "Content-Type")
        self.token = cf.get("Envconf", "token")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }
        self.respons_time_limit = cf.get("Envconf", "response_time_limit")

    def tearDown(self):
        print(self.result)

    @staticmethod
    def getFieldValue(filed, delivery_way):
        try:
            filed_value = generate_data.query_id(
                "SELECT %s FROM `sale_logistics` WHERE delivery_way = '%s' AND tracking_no != '' ORDER BY id DESC LIMIT 1" % (filed, delivery_way),
                filed)
        except Exception as e:
            print e
            return [""]
        return filed_value

    def test_courier_receipt_skynet(self):
        '''出库单 - 获取快递单号 - skynet'''
        delivery_way = 'skynet'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_XpressBees(self):
        '''出库单 - 获取快递单号 - XpressBees'''
        delivery_way = 'XpressBees'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_aramex(self):
        '''出库单 - 获取快递单号 - Aramex'''
        delivery_way = 'aramex'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_GATI(self):
        '''出库单 - 获取快递单号 - GATI'''
        delivery_way = 'GATI'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_eub(self):
        '''出库单 - 获取快递单号 - 邮政'''
        delivery_way = 'E Package'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_mxexp(self):
        '''出库单 - 获取快递单号 - mxexp'''
        delivery_way = 'mxexp'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way.upper())
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_DTDC(self):
        '''出库单 - 获取快递单号 - DTDC'''
        delivery_way = 'DTDC'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_Delhivery(self):
        '''出库单 - 获取快递单号 - Delhivery'''
        delivery_way = 'Delhivery'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_Ecom(self):
        '''出库单 - 获取快递单号 - Ecom'''
        delivery_way = 'Ecom'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_emirates_cargo_Imile(self):
        '''出库单 - 获取快递单号 - emirates_cargo_Imile'''
        delivery_way = 'emirates_cargo_Imile'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_emirates_cod_Imile(self):
        '''出库单 - 获取快递单号 - emirates_cod_Imile'''
        delivery_way = 'emirates_cod_Imile'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_saudi_arabia_cargo_Imile(self):
        '''出库单 - 获取快递单号 - saudi_arabia_cargo_Imile'''
        delivery_way = 'saudi_arabia_cargo_Imile'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_courier_receipt_saudi_arabia_cod_Imile(self):
        '''出库单 - 获取快递单号 - saudi_arabia_cod_Imile'''
        delivery_way = 'saudi_arabia_cod_Imile'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_skynet(self):
        '''出库单 - 获取已有的快递单号列表 - skynet'''
        delivery_way = 'skynet'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_XpressBees(self):
        '''出库单 - 获取已有的快递单号列表 - XpressBees'''
        delivery_way = 'XpressBees'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_aramex(self):
        '''出库单 - 获取已有的快递单号列表 - Aramex'''
        delivery_way = 'aramex'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_GATI(self):
        '''出库单 - 获取已有的快递单号列表 - GATI'''
        delivery_way = 'GATI'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_eub(self):
        '''出库单 - 获取已有的快递单号列表 - 邮政'''
        delivery_way = 'E Package'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_mxexp(self):
        '''出库单 - 获取已有的快递单号列表 - mxexp'''
        delivery_way = 'mxexp'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way.upper())
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_DTDC(self):
        '''出库单 - 获取已有的快递单号列表 - DTDC'''
        delivery_way = 'DTDC'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_Delhivery(self):
        '''出库单 - 获取已有的快递单号列表 - Delhivery'''
        delivery_way = 'Delhivery'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_Ecom(self):
        '''出库单 - 获取已有的快递单号列表 - Ecom'''
        delivery_way = 'Ecom'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_emirates_cargo_Imile(self):
        '''出库单 - 获取已有的快递单号列表 - emirates_cargo_Imile'''
        delivery_way = 'emirates_cargo_Imile'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_emirates_cod_Imile(self):
        '''出库单 - 获取已有的快递单号列表 - emirates_cod_Imile'''
        delivery_way = 'emirates_cod_Imile'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_saudi_arabia_cargo_Imile(self):
        '''出库单 - 获取已有的快递单号列表 - saudi_arabia_cargo_Imile'''
        delivery_way = 'saudi_arabia_cargo_Imile'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_get_courier_receipt_saudi_arabia_cod_Imile(self):
        '''出库单 - 获取已有的快递单号列表 - saudi_arabia_cod_Imile'''
        delivery_way = 'saudi_arabia_cod_Imile'
        sp_name = self.getFieldValue('picking_name', delivery_way)
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": sp_name[0]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        # self.assertEqual(self.result['delivery_way'], delivery_way)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_skynet(self):
        '''出库单 - 打印指定单号 - skynet'''
        delivery_way = 'skynet'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_Aramex(self):
        '''出库单 - 打印指定单号 - aramex'''
        delivery_way = 'aramex'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_GATI(self):
        '''出库单 - 打印指定单号 - GATI'''
        delivery_way = 'GATI'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_eub(self):
        '''出库单 - 打印指定单号 - E Package'''
        delivery_way = 'E Package'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_mxexp(self):
        '''出库单 - 打印指定单号 - MXEXP'''
        delivery_way = 'MXEXP'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_DTDC(self):
        '''出库单 - 打印指定单号 - DTDC'''
        delivery_way = 'DTDC'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_Delhivery(self):
        '''出库单 - 打印指定单号 - Delhivery'''
        delivery_way = 'Delhivery'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_emirates_cargo_Imile(self):
        '''出库单 - 打印指定单号 - emirates_cargo_Imile'''
        delivery_way = 'emirates_cargo_Imile'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_emirates_cod_Imile(self):
        '''出库单 - 打印指定单号 - emirates_cod_Imile'''
        delivery_way = 'emirates_cod_Imile'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_saudi_arabia_cargo_Imile(self):
        '''出库单 - 打印指定单号 - saudi_arabia_cargo_Imile'''
        delivery_way = 'saudi_arabia_cargo_Imile'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    def test_print_courier_receipt_saudi_arabia_cod_Imile(self):
        '''出库单 - 打印指定单号 - saudi_arabia_cod_Imile'''
        delivery_way = 'saudi_arabia_cod_Imile'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)

    @unittest.skip("Skip")
    def test_print_courier_receipt_Ecom(self):
        '''出库单 - 打印指定单号 - Ecom'''
        delivery_way = 'Ecom'
        filed_values = [self.getFieldValue('picking_name', delivery_way), self.getFieldValue('tracking_no', delivery_way)]
        test_url = self.base_url + "/wms/stock_picking/courier_receipt/multiple"
        payload = {"sp_name": filed_values[0], "tracking_number": filed_values[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result[delivery_way], filed_values[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.respons_time_limit)


if __name__ == '__main__':
    test_data.init_data() # 初始化接口测试数据
    unittest.main()