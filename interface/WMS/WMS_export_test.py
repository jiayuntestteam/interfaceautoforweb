# coding: utf-8

# WMS给前端，BI相关

import os
import sys
import unittest
import requests
import configparser as cparser

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
# from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)


class WMSExportTest(unittest.TestCase):

    def setUp(self):
        self.base_url = cf.get("Envconf", "WMSBaseUrl")
        self.content_type = cf.get("Envconf", "Content-Type")
        self.token = cf.get("Envconf", "token")
        self._headers = {
            "Authorization": self.token
        }
        self.partner_list = [
            'aramex',  # aramex
            'indian_Delhivery_inland',  # 印度大货：Delhivery国内段
            'indian_Delhivery_category',  # 印度大货：Delhivery（品类汇总）
            'indian_cargo_Ecom_inland',  # 印度大货：Ecom国内段
            'indian_cargo_Ecom_category',  # 印度大货：Ecom（品类汇总）
            'indian_cargo_XpressBees_inland',  # 印度大货：XpressBees国内段
            'indian_cargo_XpressBees_category',  # 印度大货：XpressBees（品类汇总）
            'mexico_cargo_Skynet_inland',  # 墨西哥大货：Skynet国内段
            'mexico_cargo_Skynet_category'  # 墨西哥大货：Skynet（品类汇总）
        ]
        self.time_periods = ['2018-03-15T00:00', '2018-03-20T00:00']

    def tearDown(self):
        print(self.result)

    def test_clearance_data_export_indian_Delhivery_inland(self):
        '''报关数据导出 indian_Delhivery_inland'''
        test_url = self.base_url + "/wms/export/aramex_hs"
        payload = {"partner": self.partner_list[1], "start_time": self.time_periods[0], "end_time": self.time_periods[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.text
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_clearance_data_export_indian_Delhivery_category(self):
        '''报关数据导出 indian_Delhivery_category'''
        test_url = self.base_url + "/wms/export/aramex_hs"
        payload = {"partner": self.partner_list[2], "start_time": self.time_periods[0], "end_time": self.time_periods[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.text
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_clearance_data_export_indian_cargo_Ecom_inland(self):
        '''报关数据导出 indian_cargo_Ecom_inland'''
        test_url = self.base_url + "/wms/export/aramex_hs"
        payload = {"partner": self.partner_list[3], "start_time": self.time_periods[0], "end_time": self.time_periods[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.text
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_clearance_data_export_indian_cargo_Ecom_category(self):
        '''报关数据导出 indian_cargo_Ecom_category'''
        test_url = self.base_url + "/wms/export/aramex_hs"
        payload = {"partner": self.partner_list[4], "start_time": self.time_periods[0], "end_time": self.time_periods[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.text
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_clearance_data_export_indian_cargo_XpressBees_inland(self):
        '''报关数据导出 indian_cargo_XpressBees_inland'''
        test_url = self.base_url + "/wms/export/aramex_hs"
        payload = {"partner": self.partner_list[5], "start_time": self.time_periods[0], "end_time": self.time_periods[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.text
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_clearance_data_export_indian_cargo_XpressBees_category(self):
        '''报关数据导出 indian_cargo_XpressBees_category'''
        test_url = self.base_url + "/wms/export/aramex_hs"
        payload = {"partner": self.partner_list[6], "start_time": self.time_periods[0], "end_time": self.time_periods[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.text
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_clearance_data_export_mexico_cargo_Skynet_inland(self):
        '''报关数据导出 mexico_cargo_Skynet_inland'''
        test_url = self.base_url + "/wms/export/aramex_hs"
        payload = {"partner": self.partner_list[7], "start_time": self.time_periods[0], "end_time": self.time_periods[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.text
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_clearance_data_export_mexico_cargo_Skynet_category(self):
        '''报关数据导出 mexico_cargo_Skynet_category'''
        test_url = self.base_url + "/wms/export/aramex_hs"
        payload = {"partner": self.partner_list[8], "start_time": self.time_periods[0], "end_time": self.time_periods[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.text
        self.assertEqual(r.status_code, requests.codes.ok)

    def test_clearance_data_export_aramex(self):
        '''报关数据导出 aramex'''
        test_url = self.base_url + "/wms/export/aramex_hs"
        payload = {"partner": self.partner_list[0], "start_time": self.time_periods[0], "end_time": self.time_periods[1]}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.text
        self.assertEqual(r.status_code, requests.codes.ok)



if __name__ == '__main__':
    # test_data.init_data() # 初始化接口测试数据
    unittest.main()