# coding: utf-8

# WMS给前端，物流通道自动选择

import os
import sys
import unittest
import requests
import configparser as cparser
import db_fixture.mysql_db as my_DB
from datetime import timedelta, date
from db_fixture import test_data

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)

# ======== Global Paras ===============
delivery_ways = []
dest_ports = []


class WMSWeighTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.base_url = cf.get("Envconf", "WMSBaseUrl")
        cls.content_type = cf.get("Envconf", "Content-Type")
        cls.token = cf.get("Envconf", "token")
        cls._headers = {
            "Content-Type": cls.content_type,
            "Authorization": cls.token
        }
        cls.response_time_limit = cf.get("Envconf", "response_time_limit")
        cls.test_DB = my_DB.DB()
        # cls.delivery_ways = ['arabia_Fetchr', 'arabia_Imile', 'arabia_Naqel', 'arabia_Smsa', 'aramex', 'Blue Dart',
        #                  'China Post', 'Delhivery', 'DTDC', 'Ecom', 'emirates_Imile', 'E Package', 'GATI', 'JCUK',
        #                  'MXEXP', 'SFMX', 'Skynet', 'XpressBees']
        # cls.dest_ports = ['BLR', 'BOM', 'DEL']

    @classmethod
    def tearDownClass(cls):
        cls.test_DB.close()
        print("End of the test WMSWeighTest")

    def tearDown(self):
        # self.test_DB.close()
        print(self.result)

    def test_weigh_small_package(self):
        """出库称重 - 包裹称重"""
        test_url = self.base_url + "/wms/sale_logistic/weigh"
        sql_for_tracking_no = "select * from sale_logistics WHERE tracking_no!='' order by id DESC limit 1"
        tracking_nos = self.test_DB.get_field_value(sql_for_tracking_no, 'tracking_no')
        for tracking_no in tracking_nos:
            data = {"tracking_no": tracking_no, "weight": "1.121"}
            r = requests.post(test_url, json=data, headers=self._headers)
            self.assertEqual(r.status_code, requests.codes.ok)
            self.result = r.json()
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    @unittest.skip("test skip")
    def test_create_large_parcel(self):
        """出库称重 - 创建大包裹"""
        test_url = self.base_url + "/wms/large_parcel"
        self.result = []
        for delivery_way in delivery_ways:
            if delivery_way in ['Delhivery', 'Ecom', 'XpressBees']:
                dest_port = dest_ports[0]
            else:
                dest_port = ''
            for is_special in [False, True]:
                data = {"delivery_way": delivery_way,
                        "dest_port": dest_port,
                        "is_special": is_special}
                r = requests.post(test_url, json=data, headers=self._headers)
                self.assertEqual(r.status_code, requests.codes.ok)
                self.result.append(r.json())
                self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_show_large_parcel(self):
        """出库称重 - 展示现有大包裹列表"""
        test_url = self.base_url + "/wms/large_parcel"
        payload = {}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_a_get_all_delivery_ways(self):
        """出库称重 - 获取系统所有物流方式"""
        global delivery_ways
        global dest_ports

        test_url = self.base_url + "/wms/delivery_way"
        payload = {}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        if len(self.result) != 0:
            for delivery_way_info in self.result['data']:
                delivery_ways.append(delivery_way_info['delivery_way'])
                if delivery_way_info['delivery_way'] == 'Ecom':
                    dest_ports = delivery_way_info['dest_ports']

    def test_get_large_parcle_info(self):
        """出库称重 - 获取大包裹详情"""
        sql_for_large_parcel_no = "select * from large_parcel order by id DESC limit 1"
        large_parcel_no = self.test_DB.get_field_value(sql_for_large_parcel_no, 'id')

        test_url = self.base_url + "/wms/large_parcel/%d" % large_parcel_no[0]
        payload = {}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_add_package_success(self):
        """出库称重 - 新建物流单号 - 成功"""
        sql_for_large_parcel_no = "SELECT * FROM large_parcel where delivery_way='XpressBees' and dest_port='BOM' ORDER BY id DESC LIMIT 1"
        large_parcel_nos = self.test_DB.get_field_value(sql_for_large_parcel_no, 'id')
        sql_for_tracking_no = "select * from sale_logistics WHERE delivery_way='XpressBees' and dest_port='BOM' " \
                              "AND weight!=0 AND large_parcel_id=0 order by id DESC limit 1"
        tracking_nos = self.test_DB.get_field_value(sql_for_tracking_no, 'tracking_no')

        if len(large_parcel_nos) != 0 and len(tracking_nos) != 0:
            test_url = self.base_url + "/wms/large_parcel/%d" % large_parcel_nos[0]
            data = {"tracking_no": tracking_nos[0]}
            r = requests.post(test_url, json=data, headers=self._headers)
            # self.assertEqual(r.status_code, requests.codes.ok)

            if r.status_code == requests.codes.bad_request:
                self.result = r.json()
                self.assertIn(u"集包失败", self.result['message'])
            else:
                self.assertEqual(r.status_code, requests.codes.ok)
                self.result = r.json()
                self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.result = "No available data"

    def test_add_package_fail_port(self):
        """出库称重 - 新建物流单号 - 港口原因失败"""
        sql_for_large_parcel_no = "SELECT * FROM large_parcel where delivery_way='XpressBees' and dest_port='BLR' ORDER BY id DESC LIMIT 1"
        large_parcel_nos = self.test_DB.get_field_value(sql_for_large_parcel_no, 'id')
        sql_for_tracking_no = "select * from sale_logistics WHERE delivery_way='XpressBees' and dest_port='BOM' " \
                              "AND weight!=0 AND large_parcel_id=0 order by id DESC limit 1"
        tracking_nos = self.test_DB.get_field_value(sql_for_tracking_no, 'tracking_no')

        if len(large_parcel_nos) != 0 and len(tracking_nos) != 0:
            test_url = self.base_url + "/wms/large_parcel/%d" % large_parcel_nos[0]
            data = {"tracking_no": tracking_nos[0]}
            r = requests.post(test_url, json=data, headers=self._headers)
            self.assertEqual(r.status_code, requests.codes.bad_request)
            self.result = r.json()
            self.assertIn(u"此目的港[BOM]跟本任务不符", self.result['message'])
        else:
            self.result = "No available data"

    def test_add_package_fail_delivery_way(self):
        """出库称重 - 新建物流单号 - 港口原因失败"""
        sql_for_large_parcel_no = "SELECT * FROM large_parcel where delivery_way='Ecom' ORDER BY id DESC LIMIT 1"
        large_parcel_nos = self.test_DB.get_field_value(sql_for_large_parcel_no, 'id')
        sql_for_tracking_no = "select * from sale_logistics WHERE delivery_way='XpressBees' and dest_port='BOM' " \
                              "AND weight!=0 AND large_parcel_id=0 order by id DESC limit 1"
        tracking_nos = self.test_DB.get_field_value(sql_for_tracking_no, 'tracking_no')

        if len(large_parcel_nos) != 0 and len(tracking_nos) != 0:
            test_url = self.base_url + "/wms/large_parcel/%d" % large_parcel_nos[0]
            data = {"tracking_no": tracking_nos[0]}
            r = requests.post(test_url, json=data, headers=self._headers)
            self.assertEqual(r.status_code, requests.codes.bad_request)
            self.result = r.json()
            self.assertIn(u"此快递方式[XpressBees]跟本任务不符", self.result['message'])
        else:
            self.result = "No available data"

    def test_add_package_fail_set(self):
        """出库称重 - 新建物流单号 - 因已集包失败"""
        sql_for_large_parcel_no = "SELECT * FROM large_parcel where delivery_way='XpressBees' and dest_port='BOM' ORDER BY id DESC LIMIT 1"
        large_parcel_nos = self.test_DB.get_field_value(sql_for_large_parcel_no, 'id')
        sql_for_tracking_no = "select * from sale_logistics WHERE delivery_way='XpressBees' and dest_port='BOM' " \
                              "AND weight!=0 AND large_parcel_id!=0 order by id DESC limit 1"
        tracking_nos = self.test_DB.get_field_value(sql_for_tracking_no, 'tracking_no')

        if len(large_parcel_nos) != 0 and len(tracking_nos) != 0:
            test_url = self.base_url + "/wms/large_parcel/%d" % large_parcel_nos[0]
            data = {"tracking_no": tracking_nos[0]}
            r = requests.post(test_url, json=data, headers=self._headers)
            self.assertEqual(r.status_code, requests.codes.bad_request)
            self.result = r.json()
            self.assertIn(u"此快递方式[XpressBees]跟本任务不符", self.result['message'])
        else:
            self.result = "No available data"

    def test_delete_package(self):
        """出库称重 - 新建物流单号 - 删除已集包小包裹"""
        sql_for_tracking_no_id = "select * from sale_logistics WHERE large_parcel_id!=0 order by id DESC limit 1"
        large_parcel_ids = self.test_DB.get_field_value(sql_for_tracking_no_id, 'large_parcel_id')
        tracking_no_ids = self.test_DB.get_field_value(sql_for_tracking_no_id, 'id')

        if len(tracking_no_ids) != 0 and len(large_parcel_ids) != 0:
            test_url = self.base_url + "/wms/large_parcel/%d?sale_logistics_id=%d" % (large_parcel_ids[0], tracking_no_ids[0])
            r = requests.delete(test_url, headers=self._headers)
            if r.status_code == requests.codes.bad_request:
                self.result = r.json()
                self.assertIn(u"删除物流单失败！大包裹创建时间超过了48小时，无法删除", self.result['message'])
            else:
                self.assertEqual(r.status_code, requests.codes.ok)
                self.result = r.json()
                self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.result = "No available data"

    def test_finish_large_parcel(self):
        """出库称重 - 结束集包"""
        sql_for_large_parcel = "select * from large_parcel WHERE customs_number=0 order by id DESC limit 1"
        large_parcel_ids = self.test_DB.get_field_value(sql_for_large_parcel, 'id')

        if len(large_parcel_ids) != 0:
            test_url = self.base_url + "/wms/large_parcel/finish/%d" % large_parcel_ids[0]
            data = {"choice": "use_auto_gen"}
            r = requests.post(test_url, json=data, headers=self._headers)
            if r.status_code == requests.codes.bad_request:
                self.result = r.json()
                self.assertIn(u"已有分单号", self.result['message'])
            else:
                self.assertEqual(r.status_code, requests.codes.ok)
                self.result = r.json()
                self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.result = "No available data"

    def test_show_choice_list(self):
        """出库称重 - 结束集包 - 弹出分单号选择框"""
        sql_for_large_parcel = "select * from large_parcel WHERE customs_number=0 order by id DESC limit 1"
        large_parcel_ids = self.test_DB.get_field_value(sql_for_large_parcel, 'id')

        if len(large_parcel_ids) != 0:
            test_url = self.base_url + "/wms/large_parcel/finish/%d" % large_parcel_ids[0]
            r = requests.get(test_url, params={}, headers=self._headers)
            self.assertEqual(r.status_code, requests.codes.ok)
            self.result = r.json()
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.result = "No available data"

    def test_download_customs_number_template(self):
        """出库称重 － 分单号模版下载"""
        test_url = self.base_url + "/wms/customs_number"
        r = requests.get(test_url, params={}, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_print_shipping_labels(self):
        """出库称重 － 补打面单"""
        sql_for_large_parcel = "select * from large_parcel WHERE customs_number!=0 order by id DESC limit 1"
        large_parcel_ids = self.test_DB.get_field_value(sql_for_large_parcel, 'id')

        if len(large_parcel_ids) != 0:
            test_url = self.base_url + "/wms/large_parcel/print/%d" % large_parcel_ids[0]
            r = requests.get(test_url, params={}, headers=self._headers)
            self.assertEqual(r.status_code, requests.codes.ok)
            self.result = r.json()
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.result = "No available data"


if __name__ == '__main__':
    test_data.init_data()  # 初始化接口测试数据
    unittest.main()
