# coding: utf-8

# WMS给前端，盘点相关

import os
import sys
import unittest
import requests
import configparser as cparser
import db_fixture.mysql_db as my_DB


def get_field_value(filed):
    try:
        filed_value = test_DB.get_field_value(
            "SELECT * FROM `stock_picking_line` where `status` = 'SP_INIT' ORDER BY id DESC LIMIT 1",
            filed)
    except Exception as e:
        print e
        return [""]
    return filed_value[0]


parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
# from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)

origin = []
lines = []
box_name = ""
# stock_in_num = 1
test_DB = my_DB.DB()
sku_id = get_field_value("sku_id")
stock_in_num = get_field_value("sp_line_req_qty")
spl_id = get_field_value("id")


class WMSStockInTest(unittest.TestCase):

    def setUp(self):
        self.base_url = cf.get("Envconf", "WMSBaseUrl")
        self.content_type = cf.get("Envconf", "Content-Type")
        self.token = cf.get("Envconf", "token")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }
        # self.sku_id = self.getFieldValue("sku_id")
        # self.stock_in_num = self.getFieldValue("sp_line_req_qty")
        # self.spl_id = self.getFieldValue("id")
        self.response_time_limit = cf.get("Envconf", "response_time_limit")

    def tearDown(self):
        print(self.result)

    def test_stock_in_info_for_sku(self):
        '''添加入库单 - 检索sku'''
        print "test_stock_in_info_for_sku"
        global lines
        test_url = self.base_url + "/wms/stock_in/line"
        payload = {"sku_id": sku_id}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        temp_r = self.result = r.json()
        temp_r['qty'] = stock_in_num
        lines.append(temp_r)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_stock_in_source(self):
        '''添加入库单 - 获取入库来源'''
        global origin
        test_url = self.base_url + "/wms/stock_in/origin"
        r = requests.get(url=test_url, headers=self._headers)
        origin = self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_stock_in_x_add(self):
        '''添加入库单 - 确认'''
        test_url = self.base_url + "/wms/stock_in/line"
        data = {
            "origin": u"退货入库",
            "lines": lines
        }
        r = requests.post(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertEqual(self.result['message'], u"入库成功")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_stock_in_manage(self):
        '''入库单管理'''
        test_url = self.base_url + "/wms/stock_in/manage"
        payload = {
            "sku_no": sku_id,
            "order_by": "-create_time",
            "page": 1,
            "size": 50
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_box_order(self):
        '''出库单 - 获取库位'''
        global box_name
        test_url = self.base_url + "/wms/box_order"
        payload = {"query": "free"}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        box_name = self.result["name"]
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_stock_in_y_for_wms(self):
        '''出库单 - 移库'''
        test_url = self.base_url + "/wms/stock_picking_line"
        data = {
            "spl_id": spl_id,
            "box_name": box_name,
            "qty": stock_in_num,
            "operation": "EVENT_SPL_ALLOCATE"
        }
        r = requests.patch(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        # self.assertEqual(self.result["message"], u"移入成功")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_stock_in_z_PDA(self):
        '''确认移入 - 扫描sku_id - 出库单'''
        test_url = self.base_url + "/wms/pda/operate/shelve"
        data = {
            "box_name": box_name,
            "sku_id": sku_id
        }
        for i in range(stock_in_num):
            r = requests.patch(url=test_url, json=data, headers=self._headers)
            self.assertEqual(r.status_code, requests.codes.ok)
            self.result = r.json()
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


if __name__ == '__main__':
    # test_data.init_data() # 初始化接口测试数据
    unittest.main()