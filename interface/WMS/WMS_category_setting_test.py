# coding: utf-8

# 商品品类配置

import os
import sys
import unittest
import requests
import configparser as cparser
import db_fixture.mysql_db as my_DB
from datetime import timedelta, date
from db_fixture import test_data

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)

# ======== Global Paras ===============
test_category = [60108, "女式毛衣", "Women's Sweaters"]
test_box_norm_id = 0
test_sku = 5270566


class WMSCategorySettingTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.base_url = cf.get("Envconf", "WMSBaseUrl")
        cls.content_type = cf.get("Envconf", "Content-Type")
        cls.token = cf.get("Envconf", "token")
        cls._headers = {
            "Content-Type": cls.content_type,
            "Authorization": cls.token
        }
        cls.response_time_limit = cf.get("Envconf", "response_time_limit")
        cls.test_DB = my_DB.DB()

    @classmethod
    def tearDownClass(cls):
        cls.test_DB.close()
        print("End of the test WMSCategorySettingTest")

    def tearDown(self):
        # self.test_DB.close()
        print(self.result)

    def test_get_category_tree(self):
        """商品品类配置 - 获取类目树"""
        test_url = self.base_url + "/wms/category/one_level_category"
        payload = {}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_children_category_tree(self):
        """商品品类配置 - 获取子类目树"""
        test_url = self.base_url + "/wms/category/children_category"
        payload = {"category_id": test_category[0]}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_box_category(self):
        """商品品类配置 - 获取类目对应库位信息"""
        test_url = self.base_url + "/wms/box_category/manage"
        payload = {"category_id": test_category[0]}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_category_tree_for_sku(self):
        """商品品类配置 - 根据sku号获取类目树"""
        test_url = self.base_url + "/wms/category/sku_category"
        payload = {"sku_id": test_sku}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_add_box_norm(self):
        """商品品类配置 - 设置类目库位信息"""
        global test_box_norm_id

        test_url = self.base_url + "/wms/box_category/manage"
        data = {"box_norm": "单层5分位",
                "category_id": test_category[0],
                "category_name_cn": test_category[1],
                "category_name_en": test_category[2]}
        r = requests.post(test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        test_box_norm_id = self.result["id"]

    def test_del_box_norm(self):
        """商品品类配置 - 设置类目库位信息"""
        test_url = self.base_url + "/wms/box_category/manage"
        data = {"id": test_box_norm_id}
        r = requests.delete(test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


if __name__ == '__main__':
    test_data.init_data()  # 初始化接口测试数据
    unittest.main()
