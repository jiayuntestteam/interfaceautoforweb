# coding: utf-8

# WMS给前端，盘点相关

import os
import sys
import unittest
import requests
import configparser as cparser
import db_fixture.mysql_db as my_DB

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
# from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)

delivery_ways_in_plan_type = {}
ready_to_out = {}


class WMSPutOutStockAllocationPlanTest(unittest.TestCase):
    def setUp(self):
        self.base_url = cf.get("Envconf", "WMSBaseUrl")
        self.content_type = cf.get("Envconf", "Content-Type")
        self.token = cf.get("Envconf", "token")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }
        self.response_time_limit = cf.get("Envconf", "response_time_limit")
        self.test_DB = my_DB.DB()
        self.plan_types = [
            "default",
            "indian_cargo_DTDC",
            "indian_cargo_Delhivery",
            "indian_cargo_Ecom",
            "indian_cargo_XpressBees",
            "indian_cod_Delhivery",
            "indian_cod_XpressBees",
            "indian_cod_Ecom",
            "mexico_cargo_Skynet"
        ]
        self.city_names = ["Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh",
                           "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana",
                           "Himachal Pradesh", "Jharkhand", "Karnataka", "Kerala", "Madhya Pradesh", "Maharashtra",
                           "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan",
                           "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand",
                           "West Bengal"]
        self.channels = ["ocean", "paytm", "payu"]

    def tearDown(self):
        print(self.result)
        self.test_DB.close()

    @staticmethod
    def getFieldValue(self, filed, sql_index):
        sqls = [
            "SELECT %s FROM `idle_sku` WHERE box_name != '' ORDER BY id DESC LIMIT 1" % filed,
            "SELECT %s FROM `idle_sku` WHERE status = 'IDLE_UNALLOCATED' ORDER BY id DESC LIMIT 1" % filed,
            "SELECT %s FROM `idle_sku` WHERE status = 'IDLE_TO_OFF_SHELF' ORDER BY id DESC LIMIT 1" % filed
        ]
        try:
            filed_value = self.test_DB.get_field_value(sqls[sql_index], filed)
        except Exception as e:
            print e
            return [""]
        return filed_value[0]

    def test_search_with_sku(self):
        '''出库单 - 检索'''
        test_url = self.base_url + "/wms/stock_picking/plan_off_shelf"
        payload = {
            "left": "",
            "right": "",
            "assignee_id": "",
            "page": 1,
            "count": 20
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_a_get_delivery_list(self):
        '''物流计划 - 获取初始数据'''
        global delivery_ways_in_plan_type
        test_url = self.base_url + "/wms/logistics_plan/init"
        for plan_type in self.plan_types:
            payload = {"plan_type": plan_type}
            r = requests.get(url=test_url, params=payload, headers=self._headers)
            self.assertEqual(r.status_code, requests.codes.ok)
            self.result = r.json()
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
            delivery_ways_in_plan_type[plan_type] = self.result["delivery_way_list"]

    def test_plan_out_count_default(self):
        '''物流计划 - 计算订单个数 - 默认物流'''
        global ready_to_out
        id_list = []
        test_url = self.base_url + "/wms/logistics_plan"
        payload = {
            "delivery_way": delivery_ways_in_plan_type[self.plan_types[0]],
            "plan_type": self.plan_types[0]
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        for i in range(self.result["count"]):
            id_list.append(self.result["data"][i]["id"])
        if len(id_list) != 0:
            ready_to_out[self.plan_types[1]] = id_list
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_plan_out_count_indian_cargo_DTDC(self):
        '''物流计划 - 计算订单个数 - 印度大货DTDC'''
        global ready_to_out
        id_list = []
        test_url = self.base_url + "/wms/logistics_plan"
        payload = {
            "delivery_way": ','.join(delivery_ways_in_plan_type[self.plan_types[1]]),
            "plan_type": self.plan_types[1],
            "states": ','.join(self.city_names)
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        for i in range(self.result["count"]):
            id_list.append(self.result["data"][i]["id"])
        if len(id_list) != 0:
            ready_to_out[self.plan_types[1]] = id_list
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_plan_out_count_indian_cargo_Delhivery(self):
        '''物流计划 - 计算订单个数 - 印度大货Delhivery'''
        global ready_to_out
        id_list = []
        test_url = self.base_url + "/wms/logistics_plan"
        payload = {
            "delivery_way": delivery_ways_in_plan_type[self.plan_types[2]],
            "plan_type": self.plan_types[2],
            "states": self.city_names,
            "channels": self.channels
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        for i in range(self.result["count"]):
            id_list.append(self.result["data"][i]["id"])
        if len(id_list) != 0:
            ready_to_out[self.plan_types[1]] = id_list
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_plan_out_count_indian_cargo_Ecom(self):
        '''物流计划 - 计算订单个数 - 印度大货Ecom'''
        global ready_to_out
        id_list = []
        test_url = self.base_url + "/wms/logistics_plan"
        payload = {
            "delivery_way": delivery_ways_in_plan_type[self.plan_types[3]],
            "plan_type": self.plan_types[3],
            "states": self.city_names,
            "channels": self.channels
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        for i in range(self.result["count"]):
            id_list.append(self.result["data"][i]["id"])
        if len(id_list) != 0:
            ready_to_out[self.plan_types[1]] = id_list
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_plan_out_count_indian_cargo_XpressBees(self):
        '''物流计划 - 计算订单个数 - 印度大货XpressBees'''
        global ready_to_out
        id_list = []
        test_url = self.base_url + "/wms/logistics_plan"
        payload = {
            "delivery_way": delivery_ways_in_plan_type[self.plan_types[4]],
            "plan_type": self.plan_types[4],
            "states": self.city_names,
            "channels": self.channels
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        for i in range(self.result["count"]):
            id_list.append(self.result["data"][i]["id"])
        if len(id_list) != 0:
            ready_to_out[self.plan_types[1]] = id_list
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_plan_out_count_indian_cod_Delhivery(self):
        '''物流计划 - 计算订单个数 - 印度cod Delhivery'''
        global ready_to_out
        id_list = []
        test_url = self.base_url + "/wms/logistics_plan"
        payload = {
            "delivery_way": delivery_ways_in_plan_type[self.plan_types[5]],
            "plan_type": self.plan_types[5]
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        for i in range(self.result["count"]):
            id_list.append(self.result["data"][i]["id"])
        if len(id_list) != 0:
            ready_to_out[self.plan_types[1]] = id_list
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_plan_out_count_indian_cod_Ecom(self):
        '''物流计划 - 计算订单个数 - 印度cod Ecom'''
        global ready_to_out
        id_list = []
        test_url = self.base_url + "/wms/logistics_plan"
        payload = {
            "delivery_way": delivery_ways_in_plan_type[self.plan_types[7]],
            "plan_type": self.plan_types[7]
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        for i in range(self.result["count"]):
            id_list.append(self.result["data"][i]["id"])
        if len(id_list) != 0:
            ready_to_out[self.plan_types[1]] = id_list
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_plan_out_count_indian_cod_XpressBees(self):
        '''物流计划 - 计算订单个数 - 印度cod XpressBees'''
        global ready_to_out
        id_list = []
        test_url = self.base_url + "/wms/logistics_plan"
        payload = {
            "delivery_way": delivery_ways_in_plan_type[self.plan_types[6]],
            "plan_type": self.plan_types[6]
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        for i in range(self.result["count"]):
            id_list.append(self.result["data"][i]["id"])
        if len(id_list) != 0:
            ready_to_out[self.plan_types[1]] = id_list
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_plan_out_count_mexico_cargo_Skynet(self):
        '''物流计划 - 计算订单个数 - 墨西哥大货 Skynet'''
        global ready_to_out
        id_list = []
        test_url = self.base_url + "/wms/logistics_plan"
        payload = {
            "delivery_way": delivery_ways_in_plan_type[self.plan_types[8]],
            "plan_type": self.plan_types[8]
        }
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        for i in range(self.result["count"]):
            id_list.append(self.result["data"][i]["id"])
        if len(id_list) != 0:
            ready_to_out[self.plan_types[1]] = id_list
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    @unittest.skipIf(len(ready_to_out) == 0, "no test_data")
    def test_plan_out_z_modify_delivery_way(self):
        """物流计划 - 修改订单的物流方式"""
        test_url = self.base_url + "/wms/logistics_plan"
        data_item = ready_to_out.items()[0]
        data = {
            "plan_type": data_item[0],
            "to_delivery_way": delivery_ways_in_plan_type[data_item[0]][0],
            "logistics_id_list": data_item[1],
            "to_plan_type": data_item[0]
        }
        r = requests.patch(url=test_url, json=data, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


if __name__ == '__main__':
    # test_data.init_data() # 初始化接口测试数据
    unittest.main()
