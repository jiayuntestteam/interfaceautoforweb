# coding: utf-8

# WMS给前端，物流通道自动选择

import os
import sys
import unittest
import requests
import configparser as cparser
import db_fixture.mysql_db as my_DB
from datetime import timedelta, date
from db_fixture import test_data

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)

# ======== Reading web_config.ini setting ===========
base_dir = str(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
base_dir = base_dir.replace('\\', '/')
file_path = base_dir + "/web_config.ini"

cf = cparser.ConfigParser()
cf.read(file_path)


# ======== Global Paras ===============



class WMSStockOutManagerTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.base_url = cf.get("Envconf", "WMSBaseUrl")
        cls.content_type = cf.get("Envconf", "Content-Type")
        cls.token = cf.get("Envconf", "token")
        cls._headers = {
            "Content-Type": cls.content_type,
            "Authorization": cls.token
        }
        cls.response_time_limit = cf.get("Envconf", "response_time_limit")
        cls.test_DB = my_DB.DB()

    @classmethod
    def tearDownClass(cls):
        cls.test_DB.close()
        print("End of the test WMSWeighTest")

    def tearDown(self):
        # self.test_DB.close()
        print(self.result)

    def test_get_all_stock_picking(self):
        """出库单管理 - 查看所有销售出库单"""
        test_url = self.base_url + "/wms/stock_out/manage"
        payload = {'stock_out_type': 'stock_picking', 'order_attribute': 'all', 'count': 20, 'page': 1}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_stock_picking_with_condition(self):
        """出库单管理 - 查看带条件筛选的销售出库单"""
        test_url = self.base_url + "/wms/stock_out/manage"
        payload = {'stock_out_type': 'stock_picking',
                   'status': 'SP_NOT_SHELVED',
                   'order_attribute': 'multi',
                   'create_fm': '2018-08-01',
                   'create_to': '2018-08-16',
                   'count': 20,
                   'page': 1}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_all_return_supplier_order(self):
        """出库单管理 - 查看所有退供出库单"""
        test_url = self.base_url + "/wms/stock_out/manage"
        payload = {'stock_out_type': 'return_supplier_order', 'order_attribute': 'all', 'count': 20, 'page': 1}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_return_supplier_order_with_condition(self):
        """出库单管理 - 查看带条件筛选的退供出库单"""
        test_url = self.base_url + "/wms/stock_out/manage"
        payload = {'stock_out_type': 'return_supplier_order',
                   'status': 'SP_NOT_SHELVED',
                   'order_attribute': 'multi',
                   'create_fm': '2018-08-01',
                   'create_to': '2018-08-16',
                   'count': 20,
                   'page': 1}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_all_transfer_order(self):
        """出库单管理 - 查看所有调拨出库单"""
        test_url = self.base_url + "/wms/stock_out/manage"
        payload = {'stock_out_type': 'transfer_order', 'order_attribute': 'all', 'count': 20, 'page': 1}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_transfer_order_with_condition(self):
        """出库单管理 - 查看带条件筛选的退供出库单"""
        test_url = self.base_url + "/wms/stock_out/manage"
        payload = {'stock_out_type': 'return_supplier_order',
                   'status': 'PLANNED_OUT',
                   'order_attribute': 'multi',
                   'destination':'yuhang',
                   'create_fm': '2018-08-01',
                   'create_to': '2018-08-16',
                   'count': 20,
                   'page': 1}
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_get_transfer_order_detail(self):
        """[调拨/退供]订单详情页"""
        sql_for_TOs = "SELECT * FROM `transfer_order` where `status` = 'PLANNED_OUT' ORDER BY id DESC LIMIT 1"
        TO_name = self.test_DB.get_field_value(sql_for_TOs, "name")

        test_url = self.base_url + "/wms/stock_out/line"
        payload = {
            "sp_name": TO_name[0]
        }
        r = requests.get(test_url, params=payload, headers=self._headers)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.result = r.json()
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


if __name__ == '__main__':
    test_data.init_data()  # 初始化接口测试数据
    unittest.main()
