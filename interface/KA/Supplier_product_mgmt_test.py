# coding=utf-8

import os
import sys
import unittest
import requests
import configparser as cparser
import read_config as readconfig
from db_fixture import supplier_test_data
from db_fixture import supplier_mysql_db


parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)


# ======== Reading web_config.ini setting ===========
localReadConfig = readconfig.ReadConfig()

sql_select_slr_ids = "SELECT slr.id FROM sku_link_rel AS slr INNER JOIN link ON slr.link_id = link.id " \
                     "WHERE	slr.stock_keep_unit_id = '2952746'" \
                     "AND link.link='http://detail.1688.com/offer/43932694010.html';"
sku_link_rel_id = supplier_test_data.query_id(sql_select_slr_ids, "id")
print sku_link_rel_id



class SupplierProductMgmtTest(unittest.TestCase):

    def setUp(self):
        self.base_url = localReadConfig.get_envconf("kabaseurl")
        self.spc_base_url = localReadConfig.get_product("spcbaseurl")
        self.content_type = localReadConfig.get_supplier("content-type")
        self.token = localReadConfig.get_supplier("token")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }
        self.response_time_limit = localReadConfig.get_supplier("response_time_limit")


    def tearDown(self):
        print(self.result)


    @classmethod
    def tearDownClass(cls):
        # 将已上架商品恢复未上架状态，已增加读取supplier数据库的配置
        sql_delete_pp = "delete from product_push where link in ('http://abc.com/cpf', 'https://www.1WAT0000009282.com') and supplier_name = '金湖智盛服装店'"
        sql_delete_slr = "delete sku_link_rel from sku_link_rel LEFT JOIN link on sku_link_rel.link_id =  link.id where link.link in ('http://abc.com/cpf', 'https://www.1WAT0000009282.com')"
        sql_delete_link = "delete from link where link in ('http://abc.com/cpf', 'https://www.1WAT0000009282.com') and company_name = '金湖智盛服装店'"
        sql_update_default = "update sku_link_rel set is_default = 1 where id = 40646470"

        supplier_test_data.delete_test_data(sql_delete_pp)
        supplier_test_data.delete_test_data(sql_delete_slr)
        supplier_test_data.delete_test_data(sql_delete_link)
        supplier_test_data.delete_test_data(sql_update_default)
        supplier_mysql_db.DB().close()
        print("\nEnd of the test SupplierProductMgmtTest")

    def test_a_a_matching_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-待匹配上架 全部 '''
        test_url = self.base_url + "/cooperator/supplier_product/recommend_shelf/list/"
        payload = {'offset': 0, 'limit': 20, 'source_type':''}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_a_b_matching_on_shelf_hot(self):
        ''' 供应商客户端给前端 商品管理-待匹配上架 热销爆款 '''
        test_url = self.base_url + "/cooperator/supplier_product/recommend_shelf/list/"
        payload = {'offset': 0, 'limit': 20, 'source_type': 1}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        if self.result["count"] >0 :
            self.assertEqual(self.result["results"][0]["source_type"], 1)
        else:
            self.assertEqual(self.result["count"], 0)


    def test_a_c_matching_on_shelf_rare(self):
        ''' 供应商客户端给前端 商品管理-待匹配上架 稀缺商品 '''
        test_url = self.base_url + "/cooperator/supplier_product/recommend_shelf/list/"
        payload = {'offset': 0, 'limit': 20, 'source_type': 2}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        if self.result["count"] > 0:
            self.assertEqual(self.result["results"][0]["source_type"], 2)
        else:
            self.assertEqual(self.result["count"], 0)


    def test_a_d_matching_on_shelf_rare(self):
        ''' 供应商客户端给前端 商品管理-待匹配上架 潜力爆款 '''
        test_url = self.base_url + "/cooperator/supplier_product/recommend_shelf/list/"
        payload = {'offset': 0, 'limit': 20, 'source_type': 3}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        if self.result["count"] > 0:
            self.assertEqual(self.result["results"][0]["source_type"], 3)
        else:
            self.assertEqual(self.result["count"], 0)


    def test_b_a_product_detail(self):
        ''' 供应商客户端给前端 商品管理-商品详情 '''
        self.sku_id_list = []
        test_url = self.base_url + "/cooperator/product/detail/"
        payload = {'item_no': 'JRI000118223N'}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn("item_no", r.content)
        self.assertIn("sku_list", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        for i in self.result["sku_list"]:
            self.sku_id_list.append(i["sku_id"])


    def test_b_b_product_detail(self):
        ''' 供应商客户端给前端 商品管理-货号不存在商品详情 '''
        test_url = self.base_url + "/cooperator/product/detail/"
        payload = {'item_no': 'AAA'}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)



    def test_b_c_product_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-货号上下架  链接重复上架失败'''
        self.test_b_a_product_detail()
        sku_id_nums = len(self.sku_id_list)
        if sku_id_nums >=2 :
            test_url = self.base_url + "/cooperator/product/on_off_shelf/1/"
            body = {
                    "item_no": "JRI000118223N",
                    "link": "https://detail.1688.com/offer/537302637275.html",
                    "price_rmb": "13.00",
                    "action": "on_shelf",
                    "sku_list":[
                        {
                            "sku_id": int(self.sku_id_list[0]),
                            "sold_out": True,
                            "price_rmb": "14.00",
                            "sku_barcode": "供货条码test"
                            },
                        {
                            "sku_id": int(self.sku_id_list[1]),
                            "sold_out": False,
                            "price_rmb": "13.00",
                            "sku_barcode": "供货条码test"
                        }
                    ]
                    }
            r = requests.post(url=test_url, json=body, headers=self._headers)
            self.result = r.json()
            self.assertEqual(r.status_code, 400)
            self.assertEqual(self.result["msg"], u'商品已上架无需再次上架')
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.assertLessEqual(0, sku_id_nums)


    def test_c_product_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-货号上下架  货号价格为负数上架失败'''
        self.test_b_a_product_detail()
        sku_id_nums = len(self.sku_id_list)
        if sku_id_nums >= 2:
            test_url = self.base_url + "/cooperator/product/on_off_shelf/1/"
            body = {
                    "item_no": "JRI000118223N",
                    "link": "https://detail.1688.com/offer/537302637275.html",
                    "price_rmb": "-13.00",
                    "action": "on_shelf",
                    "sku_list": [
                        {
                            "sku_id": int(self.sku_id_list[0]),
                            "sold_out": True,
                            "price_rmb": "14.00",
                            "sku_barcode": "供货条码test"
                        },
                        {
                            "sku_id": int(self.sku_id_list[1]),
                            "sold_out": False,
                            "price_rmb": "13.00",
                            "sku_barcode": "供货条码test"
                        }
                                ]
                    }
            r = requests.post(url=test_url, json=body, headers=self._headers)
            self.result = r.json()
            self.assertEqual(r.status_code, 400)
            self.assertEqual(self.result["msg"], u'价格错误')
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.assertLessEqual(0, sku_id_nums)


    # 后续相关功能改动时，添加该用例
    # def test_d_product_on_shelf(self):
    #     ''' 供应商客户端给前端 商品管理-货号上下架  sku价格为负数上架失败'''
    #     self.test_b_a_product_detail()
    #     sku_id_nums = len(self.sku_id_list)
    #     if sku_id_nums >= 2:
    #         test_url = self.base_url + "/cooperator/product/on_off_shelf/1/"
    #         body = {
    #                 "item_no": "JRI000118223N",
    #                 "link": "https://detail.1688.com/offer/sssss.html",
    #                 "price_rmb": "13.00",
    #                 "action": "on_shelf",
    #                 "sku_list": [
    #                     {
    #                         "sku_id": int(self.sku_id_list[0]),
    #                         "sold_out": True,
    #                         "price_rmb": "-14.00",
    #                         "sku_barcode": "供货条码test"
    #                     },
    #                     {
    #                         "sku_id": int(self.sku_id_list[1]),
    #                         "sold_out": False,
    #                         "price_rmb": "13.00",
    #                         "sku_barcode": "供货条码test"
    #                     }
    #                             ]
    #                 }
    #         r = requests.post(url=test_url, json=body, headers=self._headers)
    #         self.result = r.json()
    #         self.assertEqual(r.status_code, 400)
    #         self.assertEqual(self.result["msg"], u'价格错误')
    #         self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
    #     else:
    #         self.assertLessEqual(0, sku_id_nums)


    def test_e_product_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-货号上下架  货号为空上架失败'''
        self.test_b_a_product_detail()
        sku_id_nums = len(self.sku_id_list)
        if sku_id_nums >= 2:
            test_url = self.base_url + "/cooperator/product/on_off_shelf/1/"
            body = {
                    "item_no": "",
                    "link": "https://detail.1688.com/offer/537302637275.html",
                    "price_rmb": "13.00",
                    "action": "on_shelf",
                    "sku_list": [
                        {
                            "sku_id": int(self.sku_id_list[0]),
                            "sold_out": True,
                            "price_rmb": "14.00",
                            "sku_barcode": "供货条码test"
                        },
                        {
                            "sku_id": int(self.sku_id_list[1]),
                            "sold_out": False,
                            "price_rmb": "13.00",
                            "sku_barcode": "供货条码test"
                        }
                                ]
                    }
            r = requests.post(url=test_url, json=body, headers=self._headers)
            self.result = r.json()
            self.assertEqual(r.status_code, 400)
            self.assertEqual(self.result["msg"], u'货号错误')
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.assertLessEqual(0, sku_id_nums)


    def test_f_a_product_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-货号上下架  链接为空上架成功 '''
        self.test_b_a_product_detail()
        sku_id_nums = len(self.sku_id_list)
        if sku_id_nums >= 2:
            test_url = self.base_url + "/cooperator/product/on_off_shelf/1/"
            body = {
                "item_no": "WAT0000009282",
                "link": "",
                "price_rmb": "13.00",
                "action": "on_shelf",
                "sku_list": [
                    {
                        "sku_id": int(self.sku_id_list[0]),
                        "sold_out": True,
                        "price_rmb": "14.00",
                        "sku_barcode": "供货条码test"
                    },
                    {
                        "sku_id": int(self.sku_id_list[1]),
                        "sold_out": False,
                        "price_rmb": "13.00",
                        "sku_barcode": "供货条码test"
                    }
                ]
            }
            r = requests.post(url=test_url, json=body, headers=self._headers)
            self.result = r.json()
            self.assertEqual(r.status_code, requests.codes.ok)
            self.assertEqual(self.result["msg"], 'success')
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.assertLessEqual(0, sku_id_nums)

    def test_f_b_product_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-货号上下架  上架成功'''
        self.test_b_a_product_detail()
        sku_id_nums = len(self.sku_id_list)
        if sku_id_nums >= 2:
            test_url = self.base_url + "/cooperator/product/on_off_shelf/1/"
            body = {
                "item_no": "JRI000118223N",
                "link": "http://abc.com/cpf",
                "price_rmb": "13.00",
                "action": "on_shelf",
                "sku_list": [
                        {
                            "sku_id": int(self.sku_id_list[0]),
                            "sold_out": True,
                            "price_rmb": "14.00",
                            "total_storage": 100,
                            "sku_barcode": "供货条码test"
                        },
                        {
                            "sku_id": int(self.sku_id_list[1]),
                            "sold_out": False,
                            "price_rmb": "13.00",
                            "total_storage": 100,
                            "sku_barcode": "供货条码test"
                        }
                            ]
                    }
            r = requests.post(url=test_url, json=body, headers=self._headers)
            self.result = r.json()
            self.assertEqual(r.status_code, requests.codes.ok)
            self.assertEqual(self.result["msg"], 'success')
            self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        else:
            self.assertLessEqual(0, sku_id_nums)


    def test_g_matched_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-已匹配上架 '''
        test_url = self.base_url + "/cooperator/supplier_product/on_shelf/list/"
        payload = {'limit': 20, 'offset': 0}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn('WAT0000009282', r.content)
        self.assertIn('https://www.1WAT0000009282.com', r.content)
        self.assertIn('JRI000118223N', r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)



    def test_h_sku_sale_list(self):
        ''' 供应商客户端给前端 商品管理-销售备货-sku销售列表 '''
        test_url = self.base_url + "/cooperator/sale/sku/list/"
        payload = {'limit': 20, 'offset': 0}
        r = requests.get(url=test_url, headers={"Authorization": self.token}, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), 10)    # 该接口第一次请求较慢，产品已知更改最大请求时间为10s
        self.assertIn('item_no', r.content)
        self.assertIn("next_bid_time", r.content)
        self.assertIn("price", r.content)
        self.assertIn("offer_price", r.content)
        self.assertIn("sold_out", r.content)
        self.assertIn("week_qty", r.content)


    def test_i_sku_sale_list(self):
        ''' 缺货情况筛选：有货 '''
        test_url = self.base_url + "/cooperator/sale/sku/list/"
        payload = {'limit': 20, 'offset': 0, 'sold_out': 0}
        r = requests.get(url=test_url, headers={"Authorization": self.token}, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        if self.result["count"] >1 :
            self.assertEqual(self.result["results"][1]["sold_out"], 0)
        elif self.result["count"] ==1 :
            self.assertEqual(self.result["results"][0]["sold_out"], 0)


    def test_j_sku_sale_list(self):
        ''' 本期中标情况筛选：中标 '''
        test_url = self.base_url + "/cooperator/sale/sku/list/"
        payload = {'is_default': 1, 'limit': 20, 'offset': 0}
        r = requests.get(url=test_url, headers={"Authorization": self.token}, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        if self.result["count"] >1 :
            self.assertEqual(self.result["results"][1]["is_default"], 1)
        elif self.result["count"] ==1 :
            self.assertEqual(self.result["results"][0]["is_default"], 1)


    def test_k_sku_sale_list(self):
        ''' 本期中标情况筛选：未中标 '''
        test_url = self.base_url + "/cooperator/sale/sku/list?is_default/"
        payload = {'is_default': 0, 'limit': 20, 'offset':0}
        r = requests.get(url=test_url, headers={"Authorization": self.token},params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        if self.result["count"] >1 :
            self.assertEqual(self.result["results"][1]["is_default"], 0)
        elif self.result["count"] == 1:
            self.assertEqual(self.result["results"][0]["is_default"], 0)


    def test_l_search_by_item_no(self):
        ''' 根据货号进行搜索 '''
        test_url = self.base_url + "/cooperator/sale/sku/list/"
        payload = {'item_no': 'XXX000104271N', 'limit': 20, 'offset': 0}
        r = requests.get(url=test_url, headers={"Authorization": self.token}, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        if self.result["count"] >=1 :
            self.assertEqual(self.result["results"][1]["item_no"], "XXX000104271N")
        elif self.result["count"] == 1:
            self.assertEqual(self.result["results"][0]["item_no"], "XXX000104271N")


    def test_m_edit_sku_barcode(self):
        ''' 供应商客户端给前端 订单管理-待发货-编辑供货条码 '''
        test_url = self.base_url + "/cooperator/purchase_order/supplier_sku/"
        body = {
                "sku_id": "0002582049",
                "purchase_sku_barcode": "Apple-Test"
                }
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_n_search_by_sku_barcode(self):
        ''' 根据供货条码进行搜索 '''
        test_url = self.base_url + "/cooperator/sale/sku/list"
        payload = {'sku_supplier_barcode': 'Apple-Test', 'limit': 20, 'offset': 0}
        r = requests.get(url=test_url, headers={"Authorization": self.token}, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["results"][0]["sku_no"], "0002582049")
        self.assertEqual(self.result["results"][0]["sku_supplier_barcode"], "Apple-Test")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_o_order_by_item_no_acs(self):
        ''' 按照嘉云货号从小到大排序 '''
        test_url = self.base_url + "/cooperator/sale/sku/list/?ordering=%2Bitem_no"
        payload = {'limit': 20, 'offset': 0}
        r = requests.get(url=test_url, headers={"Authorization": self.token}, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        if self.result["count"] >= 2:
            self.assertLessEqual(self.result["results"][0]["item_no"], self.result["results"][1]["item_no"])



    def test_p_change_offer_price(self):
        ''' 供应商客户端给前端 商品管理-销售备货-商品价格信息修改 '''
        # sku=0002582049  sku_link_rel.id=40646470  item_no=XXX000104271N
        test_url = self.base_url + "/cooperator/product/link/40646470/"
        body = {
                "offer_price_rmb": 11.00
                }
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_q_cooperator_supplier_sku_off_shelf(self):
        ''' 供应商客户端给前端 商品管理-销售备货-sku下架 '''
        test_url = self.base_url + "/cooperator/supplier_sku/shelf"
        body = {
                        "sku_link_rel_id":sku_link_rel_id[0],
                        "action": "off_shelf",
                        "shelf_type": "sku"
                        }
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_r_sku_sale_list(self):
        ''' 缺货情况筛选：缺货 '''
        test_url = self.base_url + "/cooperator/sale/sku/list/"
        payload = {'sold_out': 1, 'limit': 20, 'offset': 0}
        r = requests.get(url=test_url, headers={"Authorization": self.token}, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["results"][0]["sold_out"], 1)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_s_cooperator_supplier_sku_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-销售备货-sku上架 '''
        test_url = self.base_url + "/cooperator/supplier_sku/shelf"
        body = {
                "sku_link_rel_id":sku_link_rel_id[0],
                "action": "on_shelf",
                "shelf_type": "sku"
                }
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_t_cooperator_supplier_sku_off_shelf(self):
        ''' 供应商客户端给前端 商品管理-销售备货-整款货号下架 '''
        test_url = self.base_url + "/cooperator/supplier_sku/shelf"
        body = {
                "sku_link_rel_id": sku_link_rel_id[0],
                "action": "off_shelf",
                "shelf_type": "product"
                }
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_u_cooperator_supplier_sku_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-销售备货-整款货号上架 '''
        test_url = self.base_url + "/cooperator/supplier_sku/shelf"
        body = {
                "sku_link_rel_id": sku_link_rel_id[0],
                "action": "on_shelf",
                "shelf_type": "product"
                }
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_v_search_by_sku_no(self):
        ''' 根据sku_no进行搜索 '''
        test_url = self.base_url + "/cooperator/sale/sku/list/"
        payload = {'limit': 20, 'sku_no': '0002582049', 'offset': 0}
        r = requests.get(url=test_url, params=payload, headers={'Authorization': self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["results"][0]["sku_no"], '0002582049')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_w_a_return_categoryNew_ByParentId(self):
        ''' 商品中心 获取类目by父级类目id case:返回所有品类信息 '''
        test_url = self.spc_base_url + "/new_product/categoryNew/getCategoryByParentId"
        payload = {'parentId': ''}
        response = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)
        # 测试环境没有配置有效品类和无效品类，故56个一级类目全部展示
        category_top_id = localReadConfig.get_supplier("category_name_top")
        for cti in category_top_id:
            self.assertIn(cti, response.content)


    # def test_w_b_product_self_upload_upload(self):
    #     ''' 商品中心 供应商 自主上货 上传表格 '''
    #     test_url = self.spc_base_url + "/product/self_upload/upload/"
    #     upload_file_path = base_dir + '/support/upload_test.xlsx'
    #     # files = {'file': open(upload_file_path, 'rb')}
    #     files = {'file': ('upload_test.xlsx', open(upload_file_path, 'rb'), 'application/vnd.ms-excel', {'Expires': '0'})}
    #     response = requests.post(url=test_url, files=files, headers=self._headers)
    #     self.result =response.json()
    #     self.assertEqual(response.status_code, requests.codes.ok)
    #     self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)
    #     self.assertEqual(self.result['data']['product_list'][0]['purchase_id'], 'KATEST002')



    # def test_w_c_product_self_upload_submit(self):
    #     self.test_w_b_product_self_upload_upload()




if __name__ == '__main__':
    # test_data.init_data() # 初始化接口测试数据
    unittest.main()