# coding=utf-8

import os
import sys
import unittest
import requests
import configparser as cparser
import read_config as readconfig

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
localReadConfig = readconfig.ReadConfig()


class SupplierAccountInfoTest(unittest.TestCase):

    def setUp(self):
        self.base_url = localReadConfig.get_envconf("kabaseurl")
        self.po_base_url = localReadConfig.get_envconf("pobaseurl")
        self.spc_base_url = localReadConfig.get_product("spcbaseurl")
        self.content_type = localReadConfig.get_supplier("content-type")
        self.token = localReadConfig.get_supplier("token")
        self.cookies = localReadConfig.get_po("cookies")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }
        self._cookies = {
            "warehouse": self.cookies
        }
        self.response_time_limit = localReadConfig.get_supplier("response_time_limit")

    def tearDown(self):
        print(self.result)


    def test_a_a_supplier_bill_rule_update(self):
        ''' 账单管理 供应商结算信息修改 '''
        test_url = self.po_base_url + "/supplier/rule/1"
        body = {
            "discount": 0.33,
            "pay_type": "month"
        }
        response = requests.post(url=test_url, headers=self._headers, json=body,cookies=self._cookies)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)


    def test_a_b_cooperator_bill_rule(self):
        ''' 供应商客户端给前端 账户信息-结算规则 '''
        test_url = self.base_url + "/cooperator/bill/rule/"
        response = requests.get(url=test_url, headers=self._headers)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        # 结算规则：week,half_month,month
        self.assertIn(b"month", response.content)
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)


    def test_b_return_categoryNew_ByParentId(self):
        ''' 商品中心 获取类目by父级类目id case:返回所有品类信息 '''
        test_url = self.spc_base_url + "/new_product/categoryNew/getCategoryByParentId"
        payload = {'parentId': ''}
        response = requests.post(url=test_url, params=payload, headers=self._headers)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)
        # 测试环境没有配置有效品类和无效品类，故56个一级类目全部展示
        category_top_id = localReadConfig.get_supplier("category_name_top")
        for cti in category_top_id:
            self.assertIn(cti, response.content)



    def test_c_update_cooperator_identity_basic_info(self):
        ''' 供应商客户端给前端 基本信息-更新基本信息 '''
        test_url = self.base_url + "/cooperator/identity/basic_info/"
        body = {"pan": "12345678998765432a",
                "platform": "other",
                "home_url": "https://cpfshop123.com/",
                "office_address": "北京 西城区 123",
                "corporation_name": "测试法人",
                "corporation_phone": "18638500100"
                }
        response = requests.put(url=test_url, json=body, headers=self._headers)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertIn(b"success", response.content)
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)


    def test_d_update_cooperator_identity_basic_info(self):
        ''' 供应商客户端给前端 基本信息-更新基本信息 '''
        test_url = self.base_url + "/cooperator/identity/basic_info/"
        body = {"major_category": 601,
                "other_category": "602,603",
                "company_type": "personal",
                "company_attribute": "franchiser",
                "employees_scale": "5-10",
                "last_year_sales": "51-100",
                "sold_goods_qty": 1000,
                "other_sold_platform": {"1": "环球易购", "2": "执御", "18": "AE"}
                }
        response = requests.put(url=test_url, json=body, headers=self._headers)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertIn(b"success", response.content)
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)


    def test_e_update_cooperator_identity_basic_info(self):
        ''' 供应商客户端给前端 基本信息-更新基本信息 '''
        test_url = self.base_url + "/cooperator/identity/basic_info/"
        body = {"contacts_name": "test",
                "contacts_title": "CFO",
                "contacts_phone": "18638500101",
                "contacts_qq": "123454321",
                "email": "cpfyx126@126.com",
                "shipping_address": "北京 朝阳区 什么区什么县什么街道",
                "return_address": "浙江 杭州 华星时代广场B座305",
                "return_consignee_name": "test",
                "return_consignee_phone": "12344321"
                }
        response = requests.put(url=test_url, json=body, headers=self._headers)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertIn(b"success", response.content)
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)


    def test_f_return_cooperator_identity_basic_info(self):
        ''' 供应商客户端给前端 基本信息-返回基本信息 '''
        test_url = self.base_url + "/cooperator/identity/basic_info/"
        response = requests.get(url=test_url, headers=self._headers)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['home_url'], 'https://cpfshop123.com/')
        self.assertEqual(self.result['return_address'], u'浙江 杭州 华星时代广场B座305')
        self.assertEqual(self.result['company_attribute'], 'franchiser')
        self.assertEqual(self.result['other_category'], '602,603')
        self.assertEqual(self.result['company_type'], 'personal')
        self.assertEqual(self.result['return_consignee_name'], 'test')
        self.assertEqual(self.result['platform'], 'other')
        self.assertEqual(self.result['shipping_address'], u'北京 朝阳区 什么区什么县什么街道')
        self.assertEqual(self.result['return_consignee_phone'], '12344321')
        self.assertEqual(self.result['major_category'], 601)
        self.assertEqual(self.result['email'], 'cpfyx126@126.com')
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)


    def test_g_return_cooperator_bill_bank_list(self):
        ''' 供应商客户端给前端 银行卡管理-获取银行列表 '''
        test_url = self.base_url + "/cooperator/bill/bank_list/"
        payload = {"choose": 1}
        response = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)


    def test_h_update_cooperator_bill_pay_info(self):
        ''' 供应商客户端给前端 银行卡信息-新建/修改银行卡信息 '''
        test_url = self.base_url + "/cooperator/bill/pay_info/"
        body = {"bank_name": "杭州银行",
                "deposit_bank": "杭州市嘉云支行",
                # "phone": "18888881818",
                "bank_account": "123456789666",
                "password": "123123",
                "cnaps_code": "666666666666",
                # "id_card_number": "412716000000000000",
                # "account_holder": "测试供应商"
                }
        response = requests.post(url=test_url, json=body, headers=self._headers)
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertIn(b"success", response.content)
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)

    def test_i_return_cooperator_bill_pay_info(self):
        ''' 供应商客户端给前端 银行卡管理-获取账户信息 '''
        test_url = self.base_url + "/cooperator/bill/pay_info/"
        response = requests.get(url=test_url, headers={"Authorization": self.token})
        self.result = response.json()
        self.assertEqual(response.status_code, requests.codes.ok)
        self.assertEqual(self.result['bank_name'], u'杭州银行')
        self.assertEqual(self.result['deposit_bank'], u'杭州市嘉云支行')
        self.assertEqual(self.result['bank_account'], '123456789666')
        self.assertEqual(self.result['account_holder'], u'测试供应商')
        # self.assertEqual(self.result['phone'], '18888881818')
        self.assertEqual(self.result['cnaps_code'], '666666666666')
        self.assertEqual(self.result['id_card_number'], '4***************00')
        self.assertLessEqual(response.elapsed.total_seconds(), self.response_time_limit)


if __name__ == '__main__':
    # test_data.init_data() # 初始化接口测试数据
    unittest.main()











