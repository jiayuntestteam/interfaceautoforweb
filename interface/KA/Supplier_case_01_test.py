# coding=utf-8

# 休眠商家无法操作商品任何信息，http://jira.yuceyi.com/browse/WEB-534

import os
import sys
import unittest
import requests
import configparser as cparser
import pymysql.cursors
import read_config as readconfig

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
from db_fixture import supplier_mysql_db
from db_fixture import supplier_test_data

# ======== Reading web_config.ini setting ===========
localReadConfig = readconfig.ReadConfig()


class SupplierCase01Test(unittest.TestCase):

    def setUp(self):
        self.base_url = localReadConfig.get_envconf("kabaseurl")
        self.content_type = localReadConfig.get_supplier("content-type")
        self.token = localReadConfig.get_supplier("token")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }
        self.response_time_limit = localReadConfig.get_supplier("response_time_limit")

    def tearDown(self):
        print(self.result)


    def test_a_supplier_stop_cooperator(self):
        '''入驻管理--更改供应商正式信息-内部  合作供应商暂停合作'''
        test_url = self.base_url + "/supplier/partial_update/1/"
        body = {
            "follow_user_name": "胡杰斌",
            "is_fixed_cooperate": 0
        }
        r = requests.put(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "OK")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_b_nocooperator_supplier_product_on_shelf(self):
        ''' 休眠商家不允许进行商品上架操作 '''
        test_url = self.base_url + "/cooperator/product/on_off_shelf/1/"
        body = {
            "item_no": "JRI000118223N",
            "link": "http://abc.com/cpf",
            "price_rmb": "12.34",
            "action": "on_shelf"
        }
        r = requests.post(url=test_url, json=body, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, 400)
        self.assertEqual(self.result["msg"], u'只有营业中的合作供应商才能操作')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_c_nocooperator_supplier_change_offer_price(self):
        ''' 休眠商家不允许修改报价 '''
        test_url = self.base_url + "/cooperator/product/link/3013053/"
        body = {
            "offer_price_rmb": 150.50
        }
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, 400)
        self.assertEqual(self.result["msg"], u'只有营业中的合作供应商才能操作')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_d_supplier_recover_cooperator(self):
        '''入驻管理--更改供应商正式信息-内部  合作供应商恢复合作'''
        test_url = self.base_url + "/supplier/partial_update/1/"
        body = {
            "follow_user_name": "胡杰斌",
            "is_fixed_cooperate": 1
        }
        r = requests.put(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "OK")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)



if __name__ == '__main__':
    # test_data.init_data() # 初始化接口测试数据
    unittest.main()


