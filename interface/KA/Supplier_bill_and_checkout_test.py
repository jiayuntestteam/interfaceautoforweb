# coding=utf-8

import os
import sys
import unittest
import requests
import configparser as cparser
from db_fixture import mysql_db
import read_config as readconfig

sys.path.append("..")
# import db_fixture.test_data as generate_data

# parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.insert(0, parentdir)
from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
localReadConfig = readconfig.ReadConfig()


class SupplierBillAndCheckoutTest(unittest.TestCase):

    def setUp(self):
        self.base_url = localReadConfig.get_envconf("kabaseurl")
        self.content_type = localReadConfig.get_supplier("content-type")
        self.token = localReadConfig.get_supplier("token")
        self.cookies = localReadConfig.get_supplier("cookies")
        self._headers = {
            "Authorization": self.token,
            "Content-Type": self.content_type
        }
        self.response_time_limit = localReadConfig.get_supplier("response_time_limit")
        self._cookies = {
            "warehouse": self.cookies
        }
        """ 构造测试数据 """
        test_data.init_data("bill")
        self.bill_uuids = []
        self.bill_uuids = test_data.query_id("select uuid from bill order by id desc limit 3", "uuid")

    @classmethod
    def tearDownClass(cls):
        """ 删除测试数据 """
        test_data.delete_test_data("delete payment_request from payment_request LEFT JOIN pr_bill on pr_bill.pr_id = payment_request.id where pr_bill.bill_id in (SELECT id  from bill where bill.supplier_name='金湖智盛服装店delete')")
        test_data.delete_test_data("delete pr_bill from pr_bill LEFT JOIN bill on pr_bill.bill_id = bill.id where bill.supplier_name='金湖智盛服装店delete'")
        test_data.delete_test_data("delete from bill where supplier_name = '金湖智盛服装店delete'")
        # print (self.result)
        mysql_db.DB().close()
        print("\nEnd of the test SupplierBillAndCheckoutTest")


    def test_a_cooperator_bill_list(self):
        ''' 供应商客户端给前端  账单-显示所有账单列表 '''
        payload = {'limit': 20, 'offset': 0}
        test_url = self.base_url + '/cooperator/bill/list/'
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn('agree_pay_time', r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_b_cooperator_bill_supplier_note(self):
        ''' 供应商客户端给前端  账单-编辑账单备注 '''
        body = {
            "bill_uuid": self.bill_uuids[2],
            "supplier_note": "WEBtest"
        }
        test_url = self.base_url + '/cooperator/bill/supplier_note/'
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_c_search_by_supplier_note_success(self):
        ''' 根据供应商账单备注筛选：成功 '''
        payload = {'limit':20, 'offset':0, 'supplier_note':'WEBtest'}
        test_url = self.base_url + "/cooperator/bill/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["results"][0]["supplier_note"], "WEBtest")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_d_search_by_supplier_note_failure(self):
        ''' 根据供应商账单备注筛选：失败 '''
        payload = {'limit': 20, 'offset': 0, 'supplier_note': 'failuretest'}
        test_url = self.base_url + "/cooperator/bill/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"], [])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_e_search_by_bill_uuid_success(self):
        ''' 根据账单号进行筛选：成功 '''
        payload = {'limit': 20, 'offset': 0, 'uuid': self.bill_uuids[1]}
        test_url = self.base_url + "/cooperator/bill/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 1)
        self.assertEqual(self.result["results"][0]["uuid"], self.bill_uuids[1])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_f_search_by_bill_uuid_failure(self):
        ''' 根据账单号进行筛选：失败 '''
        payload = {'limit': 20, 'offset': 0, 'uuid': 'test123456'}
        test_url = self.base_url + "/cooperator/bill/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"], [])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_g_search_by_status(self):
        ''' 未到期状态账单筛选 '''
        payload = {'limit': 20, 'offset': 0, 'pr_status': 'INIT'}
        test_url = self.base_url + "/cooperator/bill/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"][0]["pr_status"], 'INIT')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_h_search_by_status(self):
        ''' 可请款状态账单筛选 '''
        payload = {'limit': 20, 'offset': 0, 'pr_status': 'CAN_APPLY'}
        test_url = self.base_url + "/cooperator/bill/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"][0]["pr_status"], 'CAN_APPLY')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_i_search_by_status(self):
        ''' 已请款状态账单筛选 '''
        payload = {'limit': 20, 'offset': 0, 'pr_status': 'APPLIED'}
        test_url = self.base_url + "/cooperator/bill/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"][0]["pr_status"], 'APPLIED')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_j_ordering_by_create_time_asc(self):
        ''' 按照账单生成时间由远及近排序 '''
        payload = {'limit': 20, 'offset': 0, 'ordering': 'create_time'}
        test_url = self.base_url + "/cooperator/bill/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        if self.result["count"] >= 2:
            self.assertLessEqual(self.result["results"][0]["create_time"], self.result["results"][1]["create_time"])
        else:
            print ("Less than two")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_k_ordering_by_create_time_desc(self):
        ''' 按照账单生成时间由近及远排序 '''
        payload = {'limit': 20, 'offset': 0, 'ordering': '-create_time'}
        test_url = self.base_url + "/cooperator/bill/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        if self.result["count"] >= 2:
            self.assertLessEqual(self.result["results"][1]["create_time"], self.result["results"][0]["create_time"])
        else:
            print ("Less than two")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_l_bill_export_details(self):
        ''' 账单-导出本页账单明细 '''
        payload = {'bill_uuids': [self.bill_uuids[0], self.bill_uuids[1]]}
        test_url = self.base_url + "/cooperator/bill/export_details/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_m_bill_export(self):
        ''' 账单-导出本页数据 '''
        payload = {'bill_uuids': self.bill_uuids[0],
        'columns': ['uuid','pr_status','create_time','purchase_rmb','discount','freight_rmb','total_rmb','supplier_note']}
        test_url = self.base_url + "/cooperator/bill/export/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_n_bill_details(self):
        ''' 账单-账单详情 '''
        payload = {'bill_uuids': self.bill_uuids[0]}
        test_url = self.base_url + "/cooperator/bill/details/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_o_bill_payment_request(self):
        ''' 账单-待请款账单-提交请款申请 '''
        self.bill_note_uuids = test_data.query_id("select uuid from bill where supplier_note = 'WEBtest'", "uuid")
        body = {
            "bill_uuid": self.bill_note_uuids[0],
            "supplier_note": "WEBtest"}
        test_url = self.base_url + "/cooperator/bill/payment_request/"
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn("pr_uuid", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def  test_p_payment_request_list(self):
        ''' 请款单列表 '''
        payload = {'limit': 20,
                   'offset': 0,
                   'create_time_from' : '2017-10-07T00:00:00.000%2B0800'
                    }
        test_url = self.base_url + "/cooperator/bill/payment_request/"
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result["count"], 0)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_q_search_by_pr_uuid(self):
        ''' 根据请款单号进行筛选 '''
        self.pr_uuids = test_data.query_id("select uuid from payment_request order by id desc limit 3", "uuid")
        payload = {'limit': 20, 'offset': 0, 'uuid': self.pr_uuids[0]}
        test_url = self.base_url + "/cooperator/bill/payment_request/"
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["results"][0]["uuid"], self.pr_uuids[0])
        self.assertEqual(self.result["count"], 1)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_r_search_by_status_pr_submit(self):
        ''' 筛选已提交状态请款单 '''
        payload = {'limit': 20, 'offset': 0, 'status': 'PR_SUBMIT'}
        test_url = self.base_url + "/cooperator/bill/payment_request/"
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        if self.result["count"] !=0 :
            self.assertEqual(self.result["results"][0]["status"], u"PR_SUBMIT")
        else:
            return u"无已提交请款单"
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_s_search_by_status_pr_cashier_paid(self):
        ''' 筛选已打款状态请款单 '''
        payload = {'limit': 20, 'offset': 0, 'status': 'PR_CASHIER_PAID'}
        test_url = self.base_url + "/cooperator/bill/payment_request/"
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        if self.result["count"] !=0 :
            self.assertEqual(self.result["results"][0]["status"], u"PR_CASHIER_PAID")
        else:
            return u"无已打款请款单"
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_t_search_by_status_pr_under_review(self):
        ''' 筛选审核中状态请款单 '''
        reviewing_status_list = ["PR_MANAGER_PASS", "PR_FINANCE_PASS", "PR_FINANCE_REJECT", "PR_CASHIER_REJECT"]
        pur_status_list = []
        payload = {'limit': 20, 'offset': 0, 'status': 'PR_UNDER_REVIEW'}
        test_url = self.base_url + "/cooperator/bill/payment_request/"
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        for i in range(self.result["count"]):
            pur_status_list.append(self.result["results"][i]["status"])
        self.assertLessEqual(set(pur_status_list), set(reviewing_status_list))
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_u_payment_request_note(self):
        ''' 请款单-备注更改 '''
        body = {"supplier_note":"pr_WEBtest"}
        test_url = self.base_url + "/cooperator/bill/payment_request/61/"
        r = requests.put(url=test_url, json=body, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_u_payment_request_details(self):
        ''' 请款单-请款单详情 '''
        test_url = self.base_url + "/cooperator/bill/payment_request/61/"
        r = requests.get(url=test_url, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["supplier_note"], "pr_WEBtest")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_v_payment_request_export(self):
        ''' 请款单-导出本页列表数据 '''
        payload = {'limit': 20,
                   'offset': 0,
                   'columns': ['uuid','bank_name','deposit_bank','bank_account','account_holder','status','pay_time','supplier_note','purchase_note','create_time','pay_rmb','total_rmb','cnaps_code']}
        test_url = self.base_url + "/cooperator/bill/payment_request/export/"
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_w_payment_request_export_details(self):
        ''' 请款单-导出本页请款单明细 '''
        payload = {'limit': 20, 'offset': 0}
        test_url = self.base_url + "/cooperator/bill/payment_request/export_details/"
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_x_goods_money_statistic(self):
        ''' 账单-货款数据统计 '''
        test_url = self.base_url + "/cooperator/goods_money/statistic/"
        r = requests.get(url=test_url, headers=self._headers, cookies=self._cookies)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn(u"paying_rmb", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)




if __name__ == '__main__':
    # test_data.init_data() # 初始化接口测试数据
    unittest.main()
