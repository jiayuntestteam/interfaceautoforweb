#  coding=utf-8

import os
import sys
import unittest
import requests
import configparser as cparser
import read_config as readconfig

# from unittest import TestCase
from db_fixture import mysql_db
from db_fixture import supplier_mysql_db

# TestCase.maxDiff = None
# unittest.util._MAX_LENGTH=1000

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
from db_fixture import test_data
from db_fixture import supplier_test_data

# ======== Reading web_config.ini setting ===========
localReadConfig = readconfig.ReadConfig()

""" 构造测试数据 """
test_data.init_data('purchase_order_ext')

sql_select_uuids = "select uuid from purchase_order order by id desc limit 4;"
po_uuids = test_data.query_id(sql_select_uuids, "uuid")
print po_uuids
print po_uuids[2]

sql_select_po_ids = "select id from purchase_order order by id desc limit 4;"
po_ids = test_data.query_id(sql_select_po_ids, "id")
print po_ids
print po_ids[2]

sql_select_sku_ids = "SELECT sku_id FROM `purchase_order` ORDER BY id DESC LIMIT 4;"
sku_ids = test_data.query_id(sql_select_sku_ids, "sku_id")
print sku_ids
print sku_ids[2]

sql_select_slr_ids = "SELECT slr.id FROM sku_link_rel AS slr INNER JOIN link ON slr.link_id = link.id " \
                     "WHERE	slr.stock_keep_unit_id = '2952746'" \
                     "AND link.link='http://detail.1688.com/offer/43932694010.html';"
sku_link_rel_id = supplier_test_data.query_id(sql_select_slr_ids, "id")
print sku_link_rel_id


class SupplierOrderPendingDeliveryTest(unittest.TestCase):

    def setUp(self):
        self.base_url = localReadConfig.get_envconf("kabaseurl")
        self.content_type = localReadConfig.get_supplier("content-type")
        self.token = localReadConfig.get_supplier("token")
        self.cookies = localReadConfig.get_supplier("cookies")
        self._headers = {
            "Authorization": self.token,
            "Content-Type": self.content_type
        }
        self.response_time_limit = localReadConfig.get_supplier("response_time_limit")
        self._cookies = {
            "warehouse": self.cookies
        }

    @classmethod
    def tearDownClass(cls):
        global po_ids
        # 删除因调用接口产生的补单数据
        delete_backorder_sql = "delete from purchase_order where top_source_purchase_order_uuid in (%s,%s,%s,%s)" % (po_uuids[0], po_uuids[1], po_uuids[2], po_uuids[3])
        test_data.delete_test_data(delete_backorder_sql)
        # 删除生成的purchase_order测试数据
        delete_po_sql = "delete from purchase_order where id in(%s,%s,%s,%s)" % (po_ids[0], po_ids[1], po_ids[2], po_ids[3])
        test_data.delete_test_data(delete_po_sql)
        # 删除因调用接口产生的供应商备注数据
        detele_po_note_sql = "delete from purchase_order_notes where supplier_note='WEBnote'"
        test_data.delete_test_data(detele_po_note_sql)

        mysql_db.DB().close()
        supplier_mysql_db.DB().close()
        print("\nEnd of the test SupplierOrderPendingDeliveryTest")



    def test_a_cooperator_purchase_order_summary(self):
        ''' 供应商客户端给前端  订单管理-待发货-统计信息 '''
        global unshipped_num
        test_url = self.base_url + "/cooperator/purchase_order/summary/"
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn(u'total', r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)
        unshipped_num = self.result["total"]


    def test_b_purchase_order_unshipped_list(self):
        ''' 订单管理-待发货 '''
        payload = {'limit': 50, 'offset': 0}
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/list/"
        r = requests.get(url=test_url, headers={"Authorization": self.token}, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], unshipped_num)
        self.assertIn(u"uuid", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_c_purchase_order_supplier_sku(self):
        ''' 订单管理-待发货-编辑供货条码 '''
        body = {
            "sku_id": sku_ids[2],
            "po_id": po_ids[2],
            "purchase_sku_barcode": "WEBtest"
        }
        test_url = self.base_url + "/cooperator/purchase_order/supplier_sku/"
        r = requests.post(url=test_url, json=body, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_d_search_by_purchase_sku_barcode_success(self):
        ''' 根据供货条码进行搜索-成功 '''
        payload = {'limit': 50, 'offset': 0, 'purchase_sku_barcode': "WEBtest"}
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"][0]["purchase_sku_barcode"], "WEBtest")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_e_search_by_purchase_sku_barcode_fail(self):
        ''' 根据供货条码进行搜索-失败 '''
        payload = {'limit': 50, 'offset': 0, 'purchase_sku_barcode': "no_possible"}
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 0)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_f_search_by_uuid_success(self):
        ''' 根据采购批次号进行搜索-成功 '''
        payload = {'limit': 50, 'offset': 0,'uuid': po_uuids[2]}
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 1)
        self.assertEqual(self.result["results"][0]["purchase_sku_barcode"], "WEBtest")
        self.assertEqual(self.result["results"][0]["uuid"], po_uuids[2])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_g_search_by_uuid_fail(self):
        ''' 根据采购批次号进行搜索-失败 '''
        payload = {'limit': 50, 'offset': 0,'uuid': 'no_possible'}
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 0)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_h_cooperator_purchase_order_supplier_note(self):
        ''' 订单管理-待发货/已发货-供应商添加备注 '''
        body = {
            "po_id": po_ids[2],
            "supplier_note": "WEBnote"
        }
        test_url = self.base_url + "/cooperator/purchase_order/supplier_note/"
        r = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_i_search_by_supplier_note_success(self):
        ''' 根据供应商备注进行搜索-成功 '''
        payload = {'limit': 50, 'offset': 0, "supplier_note": "WEBnote"}
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"][0]["supplier_note"], "WEBnote")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_j_search_by_supplier_note_fail(self):
        ''' 根据供应商备注进行搜索-失败 '''
        payload = {'limit': 50, 'offset': 0, "supplier_note": "no_possible"}
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 0)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_k_order_by_purchase_sku_barcode_asc(self):
        ''' 按供货条码从小到大排序 '''
        payload = {'limit': 50, 'offset': 0, 'ordering': 'purchase_sku_barcode'}
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        if self.result["count"] >= 2:
            self.assertLessEqual(self.result["results"][0]["purchase_sku_barcode"], self.result["results"][1]["purchase_sku_barcode"])
        else:
            print ("Less than two")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_l_order_by_purchase_sku_barcode_desc(self):
        ''' 按供货条码从大到小排序 '''
        payload = {'limit': 50, 'offset': 0, 'ordering': 'purchase_sku_barcode'}
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/list/"
        r = requests.get(url=test_url, params=payload, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        if self.result["count"] >= 2:
            self.assertLessEqual(self.result["results"][1]["purchase_sku_barcode"], self.result["results"][0]["purchase_sku_barcode"])
        else:
            print ("Less than two")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_m_download_address_lodop(self):
        ''' 订单管理-获取lodop下载地址 '''
        test_url = self.base_url + "/cooperator/download_address/lodop/"
        r = requests.get(url=test_url, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn(u"http://prod-web-tools.oss-cn-hangzhou.aliyuncs.com/CLodop_Setup_for_Win32NT.exe?", self.result["url"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_n_purchase_order_unshipped_export(self):
        ''' 订单管理-待发货-导出数据 '''
        test_url = self.base_url + "/cooperator/purchase_order/unshipped/export/"
        payload = {'limit': 20 , 'offset': 0, 'uuid': po_uuids[2]}
        r = requests.get(url=test_url, params=payload, headers=self._headers, cookies=self._cookies)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_o_cooperator_purchase_order_all_outstock_sku(self):
        ''' 订单管理-待发货-全部缺货 SKU缺货'''
        body = {
            "po_id": po_ids[2],
            "sold_out": True,
            "stock_out_note": u"从未出售该商品",
            "stock_out_type": "sku"
        }
        test_url = self.base_url + "/cooperator/purchase_order/all_outstock/"
        r = requests.post(url=test_url, headers=self._headers, cookies=self._cookies, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_p_cooperator_supplier_sku_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-销售备货-sku上架 '''
        test_url = self.base_url + "/cooperator/supplier_sku/shelf"
        body = {
                "sku_link_rel_id":sku_link_rel_id[0],
                "action": "on_shelf",
                "shelf_type": "sku"
                }
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_q_cooperator_purchase_order_all_outstock_product(self):
        ''' 订单管理-待发货-全部缺货 货号缺货 '''
        body = {
            "po_id": po_ids[3],
            "sold_out": True,
            "stock_out_note": u"价格不符",
            "stock_out_type": "product"
        }
        test_url = self.base_url + "/cooperator/purchase_order/all_outstock/"
        r = requests.post(url=test_url, headers=self._headers, cookies=self._cookies, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_r_cooperator_supplier_sku_on_shelf(self):
        ''' 供应商客户端给前端 商品管理-销售备货-整款货号上架 '''
        test_url = self.base_url + "/cooperator/supplier_sku/shelf"
        body = {
                "sku_link_rel_id": sku_link_rel_id[0],
                "action": "on_shelf",
                "shelf_type": "product"
                }
        r = requests.post(url=test_url, headers=self._headers, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)



if __name__ == '__main__':
    unittest.main()
















