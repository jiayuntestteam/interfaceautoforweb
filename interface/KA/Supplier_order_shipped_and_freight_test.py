# coding=utf-8
import os
import sys
import unittest
import requests
import configparser as cparser
import read_config as readconfig
from datetime import *
from db_fixture import mysql_db

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
localReadConfig = readconfig.ReadConfig()

""" 构造测试数据 """
test_data.init_data('purchase_order_ext')

sql_select_uuids = "select uuid from purchase_order order by id desc limit 4;"
po_uuids = test_data.query_id(sql_select_uuids, "uuid")

sql_select_po_ids = "select id from purchase_order order by id desc limit 4;"
po_ids = test_data.query_id(sql_select_po_ids, "id")

sql_select_sku_ids = "SELECT sku_id FROM purchase_order ORDER BY id DESC LIMIT 4);"
sku_ids = test_data.query_id(sql_select_sku_ids, "sku_id")
print po_uuids
print po_ids
print sku_ids



class SupplierOrderShippedAndFreightTest(unittest.TestCase):

    def setUp(self):
        self.base_url = localReadConfig.get_envconf("kabaseurl")
        self.content_type = localReadConfig.get_supplier("content-type")
        self.token = localReadConfig.get_supplier("token")
        self.cookies = localReadConfig.get_supplier("cookies")
        self._headers = {
                "Authorization": self.token,
                "Content-Type": self.content_type
            }
        self.response_time_limit = localReadConfig.get_supplier("response_time_limit")
        self._cookies = {
                "warehouse": self.cookies
            }

    @classmethod
    def tearDownClass(cls):
        global po_ids
        # 删除生成的purchase_order测试数据
        delete_po_sql = "delete from purchase_order where id in(%s,%s,%s,%s)" % (po_ids[0], po_ids[1], po_ids[2], po_ids[3])
        test_data.delete_test_data(delete_po_sql)
        # 删除因调用接口产生的供应商备注数据
        detele_po_note_sql = "delete from purchase_order_notes where supplier_note='WEBnote'"
        test_data.delete_test_data(detele_po_note_sql)
        # 删除因调用接口产生的快递数据
        delete_shipping_ticket_sql = "delete from shipping_ticket where tracking_no like '%WEBshipping123%'"
        test_data.delete_test_data(delete_shipping_ticket_sql)

        mysql_db.DB().close()
        print("\nEnd of the test SupplierOrderShippedAndFreightTest")


    def test_a_purchase_order_supplier_sku(self):
        ''' 订单管理-待发货-编辑供货条码 '''
        body = {
            "sku_id": sku_ids[3],
            "po_id": po_ids[3],
            "purchase_sku_barcode": "WEBtest"
        }
        test_url = self.base_url + "/cooperator/purchase_order/supplier_sku/"
        r = requests.post(url=test_url, json=body, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_b_cooperator_express_company_list(self):
        ''' 订单管理-支持的快递公司列表 '''
        test_url = self.base_url + "/cooperator/express_company/list/"
        r = requests.get(url=test_url, headers={"Authorization": self.token})
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn('圆通', self.result)
        self.assertIn('顺丰', self.result)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_c_cooperator_purchase_order_batch_shipped(self):
        ''' 订单管理-待发货-确认发货（支持批量） '''
        test_url = self.base_url + "/cooperator/purchase_order/batch_shipped/"
        body = {
            "tracking_no": "WEBshipping123",
            "carrier_name": "圆通",
            "freight_rmb": "10.00",
            "weight_kg": "1.000",
            "purchase_orders": [{"po_id": po_ids[3], "qty_purchased": 1}]
        }
        r = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = r.json()
        tracking_nos = test_data.query_id("select tracking_no from shipping_ticket order by id desc limit 1", "tracking_no")
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertEqual("WEBshipping123", tracking_nos[0])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_d_cooperator_purchaser_order_shipped_list(self):
        ''' 供应商客户端给前端 订单管理-已发货 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        # payload = {"purchase_time_from": time.strftime('%Y-%m-%d', time.localtime(time.time()-int(30*24*60*60))),
        #            "purchase_time_to": time.strftime('%Y-%m-%d', time.localtime(time.time()+int(24*60*60))),
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn(u"uuid", r.content)
        self.assertLessEqual(1, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_e_search_by_po_uuid_success(self):
        ''' 根据批次号进行筛选-成功 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'uuid': po_uuids[3]
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 1)
        self.assertEqual(po_uuids[3], self.result["results"][0]["uuid"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_f_search_by_po_uuid_fail(self):
        ''' 根据批次号进行筛选-失败 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {
            "purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
            "purchase_time_to": datetime.combine(date.today(), time.max),
            "limit": 50,
            'offset': 0,
            'uuid': po_uuids[2]
            }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 0)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_g_search_by_tracking_no_success(self):
        ''' 根据快递单号进行筛选-成功 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {
            "purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
            "purchase_time_to": datetime.combine(date.today(), time.max),
            "limit": 50,
            'offset': 0,
            'tracking_no': 'WEBshipping123'
        }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"][0]["shipping_tickets"][0]["tracking_no"], 'WEBshipping123')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_h_search_by_tracking_no_fail(self):
        ''' 根据快递单号进行筛选-失败 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {
            "purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
            "purchase_time_to": datetime.combine(date.today(), time.max),
            "limit": 50,
            'offset': 0,
            'tracking_no': 'no_possible'
        }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 0)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_i_search_by_sku_barcode_success(self):
        ''' 根据供货条码进行筛选-成功 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {
            "purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
            "purchase_time_to": datetime.combine(date.today(), time.max),
            "limit": 50,
            'offset': 0,
            'purchase_sku_barcode': 'WEBtest'
        }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"][0]["purchase_sku_barcode"], 'WEBtest')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_j_search_by_sku_barcode_fail(self):
        ''' 根据供货条码进行筛选-失败 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {
            "purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
            "purchase_time_to": datetime.combine(date.today(), time.max),
            "limit": 50,
            'offset': 0,
            'purchase_sku_barcode': 'no_possible'
        }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 0)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_k_cooperator_purchase_order_supplier_note(self):
        ''' 订单管理-待发货/已发货-供应商添加备注 '''
        body = {
            "po_id": po_ids[3],
            "supplier_note": "WEBnote"
        }
        test_url = self.base_url + "/cooperator/purchase_order/supplier_note/"
        r = requests.post(url=test_url, json=body, headers=self._headers, cookies=self._cookies)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_l_search_by_po_supplier_note_success(self):
        ''' 根据供应商备注进行筛选-成功 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {
            "purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
            "purchase_time_to": datetime.combine(date.today(), time.max),
            "limit": 50,
            'offset': 0,
            'supplier_note': 'WEBnote'
        }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result["count"], 0)
        self.assertEqual(self.result["results"][0]["supplier_note"], 'WEBnote')
        self.assertEqual(self.result["results"][0]["uuid"], po_uuids[3])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_m_search_by_po_supplier_note_fail(self):
        ''' 根据供应商备注进行筛选-成功 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {
            "purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
            "purchase_time_to": datetime.combine(date.today(), time.max),
            "limit": 50,
            'offset': 0,
            'supplier_note': 'no_possible'
        }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 0)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_n_po_shipped_export(self):
        ''' 订单管理-已发货-导出所有数据（不分页） '''
        test_url = self.base_url + "/cooperator/purchase_order/shipped/export/"
        payload = {"supplier_note": 'WEBnote', 'limit': 50, 'offset': 0}
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_o_change_cooperator_shipping_ticket(self):
        ''' 订单管理-已发货-修改PO单的物流信息 '''
        shipping_ticket_ids = test_data.query_id("select id from shipping_ticket where tracking_no='WEBshipping123'", "id")
        test_url = self.base_url + "/cooperator/shipping_ticket/"
        body = {
            "tracking_no": "WEBshipping123test",
            "carrier_name": "圆通",
            "freight_rmb": "10.00",
            "weight_kg": "1.000",
            "shipping_ticket_id": shipping_ticket_ids[0],
            "purchase_order_id": po_ids[3]
        }
        r = requests.put(url=test_url, json=body, headers={"Authorization": self.token}, cookies=self._cookies)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn("id", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_p_add_cooperator_shipping_ticket(self):
        ''' 订单管理-已发货-批量添加快递单号（补发） '''
        test_url = self.base_url + "/cooperator/shipping_ticket/"
        body = {
            "tracking_no": "testWEBshipping123",
            "carrier_name": "申通",
            "purchase_orders": [po_ids[3]],
            "freight_rmb": 1.00,
            "weight_kg": 5.000
        }
        r = requests.post(url=test_url, json=body, headers={"Authorization": self.token}, cookies=self._cookies)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn("id", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_q_po_shipped_receiving_list(self):
        ''' 筛选待收货状态的PO单 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'state': 'receiving'
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn(u"uuid", r.content)
        self.assertLessEqual(1, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_r_po_shipped_wait_bill_list(self):
        ''' 筛选已收货未到账期状态的PO单 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'state': 'wait_bill'
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_s_po_shipped_wait_pay_list(self):
        ''' 筛选待请款状态的PO单 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'state': 'wait_pay'
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_t_po_shipped_paying_list(self):
        ''' 筛选待收款状态的PO单 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'state': 'paying'
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_u_po_shipped_paid_list(self):
        ''' 筛选已收款状态的PO单 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'state': 'paid'
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_v_po_has_goods_return_list(self):
        ''' 筛选有退货PO单 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'has_goods_return': True
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_w_po_not_has_goods_return_list(self):
        ''' 筛选无退货PO单 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'has_goods_return': False
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_x_po_missing_package_list(self):
        ''' 筛选有少件PO单 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'missing_package': True
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_y_a_po_not_missing_package_list(self):
        ''' 筛选无少件PO单 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'missing_package': False
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_y_b_po_collected_timeout_list(self):
        ''' 筛选揽收超时 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'logistics_exception_status': "collected_timeout"
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_y_c_po_signed_timeout_list(self):
        ''' 筛选签收超时 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'logistics_exception_status': "signed_timeout"
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_y_d_po_express_timeout_list(self):
        ''' 筛选疑似丢件 '''
        test_url = self.base_url + "/cooperator/purchaser_order/shipped/list/"
        payload = {"purchase_time_from": datetime.combine((datetime.now() - timedelta(days=30)), time.min),
                   "purchase_time_to": datetime.combine(date.today(), time.max),
                   "limit": 50,
                   'offset': 0,
                   'logistics_exception_status': "express_timeout"
                   }
        r = requests.get(url=test_url, headers={"Authorization": self.token}, cookies=self._cookies, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_z_a_cooperator_bill_pay_shipping_ticket(self):
        ''' 订单管理-运费修改-显示未入账物流单-快递单号检索'''
        test_url = self.base_url + "/cooperator/bill/pay_shipping_ticket/"
        pay_load = {'limit': 20, 'offset': 0, 'pay_status': 'NOT_IN_BILL', 'tracking_no': 'WEBshipping123test'}
        r = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=pay_load)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["results"][0]["tracking_no"], 'WEBshipping123test')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_z_b_order_by_freight_rmb_asc(self):
        ''' 运费修改页面按照运费从小到大排序 '''
        test_url = self.base_url + "/cooperator/bill/pay_shipping_ticket/"
        pay_load = {'limit': 20, 'offset': 0, 'pay_status': 'NOT_IN_BILL', 'ordering': 'freight_rmb'}
        r = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=pay_load)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        if self.result["count"] >= 2:
            self.assertLessEqual(self.result["results"][0]["freight_rmb"], self.result["results"][1]["freight_rmb"])
        else:
            print ("Less than two")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_z_c_order_by_freight_rmb_desc(self):
        ''' 运费修改页面按照运费从小到大排序 '''
        test_url = self.base_url + "/cooperator/bill/pay_shipping_ticket/"
        pay_load = {'limit': 20, 'offset': 0, 'pay_status': 'NOT_IN_BILL', 'ordering': '-freight_rmb'}
        r = requests.get(url=test_url, headers=self._headers, cookies=self._cookies, params=pay_load)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        if self.result["count"] >= 2:
            self.assertLessEqual(self.result["results"][1]["freight_rmb"], self.result["results"][0]["freight_rmb"])
        else:
            print ("Less than two")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_z_d_cooperator_bill_pay_shipping_ticket(self):
        ''' 订单管理-运费修改-更改运单重量、运费信息 '''
        shipping_ticket_ids = test_data.query_id("select id from shipping_ticket where tracking_no = 'WEBshipping123test'", "id")
        test_url = self.base_url + "/cooperator/bill/pay_shipping_ticket/" + str(shipping_ticket_ids[0]) + "/"
        body = {
            "freight_rmb": 11,
            "weight_kg": 1.1
        }
        r = requests.put(url=test_url, headers=self._headers, cookies=self._cookies, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)



if __name__ == '__main__':
    unittest.main()


