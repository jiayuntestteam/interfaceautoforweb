# coding=utf-8
import os
import sys
import unittest
import requests
import configparser as cparser
import read_config as readconfig
from db_fixture import mysql_db

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
from db_fixture import test_data

# ======== Reading web_config.ini setting ===========
localReadConfig = readconfig.ReadConfig()

""" 构造测试数据 """
test_data.init_data('goods_return')
application_ids = test_data.query_id("select application_id from goods_return where application_id='RT0006'", "application_id")
po_uuids = test_data.query_id("select uuid from purchase_order INNER JOIN (select goods_return.purchase_order_id FROM goods_return WHERE application_id = 'RT0006' )as gsr ON gsr.purchase_order_id = purchase_order.id", "uuid")
purchase_sku_barcodes = test_data.query_id("select purchase_sku_barcode from purchase_order INNER JOIN (select goods_return.purchase_order_id FROM goods_return WHERE application_id = 'RT0006' )as gsr ON gsr.purchase_order_id = purchase_order.id", "purchase_sku_barcode")
purchase_links = test_data.query_id("select purchase_link from purchase_order INNER JOIN (select goods_return.purchase_order_id FROM goods_return WHERE application_id = 'RT0006' )as gsr ON gsr.purchase_order_id = purchase_order.id", "purchase_link")
goods_return_ids = test_data.query_id("select id from goods_return where application_id='RT0006'", 'id')


class SupplierReturnTracking(unittest.TestCase):

    def setUp(self):
        self.base_url = localReadConfig.get_envconf("pobaseurl")
        self.content_type = localReadConfig.get_supplier("content-type")
        self.token = localReadConfig.get_supplier("token")
        self.cookies = localReadConfig.get_supplier("cookies")
        self._headers = {
            "Authorization": self.token,
            "Content-Type": self.content_type
        }
        self.response_time_limit = localReadConfig.get_supplier("response_time_limit")
        self._cookies = {
            "warehouse": self.cookies
        }


    @classmethod
    def tearDownClass(cls):
        delete_good_return_sql = "delete from goods_return where application_id in(%s,%s,%s,%s,%s,%s,%s,%s,%s)" % ("'RT0000'", "'RT0010'", "'RT0001'", "'RT0002'", "'RT0021'", "'RT0022'", "'RT0023'", "'RT0003'", "'RT0006'")
        test_data.delete_test_data(delete_good_return_sql)
        mysql_db.DB().close()
        print("\nEnd of the test SupplierReturnTracking")

    def test_a_a_goods_return_wait_commit(self):
        ''' 订单管理-退货跟踪 待供应商确认'''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20, 'offset': 0, 'status': 'SR_WAIT_SUPPLIER_COMMIT'}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertIn("application_id", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_a_b_goods_return_wait_warehouse_return(self):
        ''' 订单管理-退货跟踪 待仓库退货'''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20, 'offset': 0, 'status': 'SR_WAIT_WAREHOUSE_RETURN'}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertIn("application_id", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_a_c_goods_return_return_finished(self):
        ''' 订单管理-退货跟踪 已退货'''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20, 'offset': 0, 'status': 'SR_RETURN_FINISHED'}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertIn("application_id", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_a_d_goods_return_supplier_abandoned(self):
        ''' 订单管理-退货跟踪 供应商已报废'''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20, 'offset': 0, 'status': 'SR_SUPPLIER_ABANDONED'}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertIn("application_id", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    def test_a_cooperator_goods_return_list(self):
        ''' 订单管理-退货跟踪 全部'''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20, 'offset': 0, 'status': ['SR_WAIT_SUPPLIER_COMMIT','SR_WAIT_WAREHOUSE_RETURN',
                                                        'SR_RETURN_FINISHED','SR_SUPPLIER_ABANDONED']}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertIn("application_id", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_b_search_by_application_id(self):
        ''' 根据退货ID检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'application_id': application_ids[0],
                   'status': ['SR_WAIT_SUPPLIER_COMMIT', 'SR_WAIT_WAREHOUSE_RETURN',
                              'SR_RETURN_FINISHED', 'SR_SUPPLIER_ABANDONED']}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["count"], 1)
        self.assertEqual(self.result["results"][0]["application_id"], application_ids[0])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_c_search_by_po_uuid(self):
        ''' 根据批次号检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'uuid': po_uuids[0]}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(1, self.result["count"])
        self.assertEqual(self.result["results"][0]["uuid"], po_uuids[0])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_d_search_by_purchase_sku_barcode(self):
        ''' 根据供货条码检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'purchase_sku_barcode': purchase_sku_barcodes[0]}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertEqual(self.result["results"][0]["purchase_sku_barcode"], purchase_sku_barcodes[0])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_e_search_by_purchase_link(self):
        ''' 根据商品链接检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'purchase_link': purchase_links[0]}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertEqual(self.result["results"][0]["purchase_link"], purchase_links[0])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_f_goods_return_supplier_return_note(self):
        ''' 订单管理-退货-添加备注 '''
        test_url = self.base_url + "/cooperator/goods_return/supplier_return_note/"
        body = {
            "id": goods_return_ids[0],
            "note": "WEBreturn"
        }
        r = requests.post(url=test_url, headers=self._headers, cookies=self._cookies, json=body)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["msg"], "success")
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_g_search_by_return_note(self):
        ''' 根据备注进行检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'return_note': 'WEBreturn'}
        r = requests.get(url=test_url, params=payload, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["results"][0]["return_note"], 'WEBreturn')
        self.assertLessEqual(1, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)

    # 需要增加添加return_reason的接口，退货原因检索的接口，快递信息的接口
    # def test_h_goods_return_export(self):
    #     ''' 订单管理-退货-导出数据 '''
    #     test_url = self.base_url + "/cooperator/goods_return/export/"
    #     payload = {'return_id': goods_return_ids[0],
    #                'columns': ['application_id','uuid','purchase_sku_barcode','sku_image_url','sku_no','purchase_link','purchase_link_note','return_quantity','purchase_price','return_note','create_time','return_reason','return_evidence','shipping_tickets','freight_rmb','status']
    #                }
    #     r = requests.get(url=test_url, params=payload, headers=self._headers)
    #     self.assertEqual(r.status_code, requests.codes.ok)
    #     self.assertLessEqual(r.elapsed.total_seconds(), 2)


    def test_i_search_by_create_time(self):
        ''' 根据创建时间检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'create_time_from': '2018-05-09'}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual('2018-05-09', self.result["results"][0]["create_time"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_j_search_by_status(self):
        ''' 待处理-检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'status': u'待处理'}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_k_search_by_status(self):
        ''' 退货中-检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'status': u'退货中'}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_l_search_by_status(self):
        ''' 已退货-检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'status': u'已退货'}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_m_search_by_status(self):
        ''' 已取消-检索 '''
        test_url = self.base_url + "/cooperator/goods_return/list/"
        payload = {'limit': 20,
                   'offset': 0,
                   'status': u'已取消'}
        r = requests.get(url=test_url, headers=self._headers, params=payload)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertLessEqual(0, self.result["count"])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)



if __name__ == '__main__':
    unittest.main()

