# coding=utf-8

import os
import sys
import unittest
import requests
import configparser as cparser
import read_config as readconfig

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parentdir)
from db_fixture import supplier_test_data
from db_fixture import supplier_mysql_db


# ======== Reading web_config.ini setting ===========
localReadConfig = readconfig.ReadConfig()


class SupplierHomePageTest(unittest.TestCase):

    def setUp(self):
        self.base_url = localReadConfig.get_envconf("kabaseurl")
        self.content_type = localReadConfig.get_supplier("content-type")
        self.token = localReadConfig.get_supplier("token")
        self.cookies = localReadConfig.get_supplier("cookies")
        self._headers = {
            "Content-Type": self.content_type,
            "Authorization": self.token
        }
        self.response_time_limit = localReadConfig.get_supplier("response_time_limit")
        self._cookies = {
            "warehouse": self.cookies
        }


    def tearDown(self):
        print(self.result)

    @classmethod
    def tearDownClass(cls):
        supplier_mysql_db.DB().close()
        print("\nEnd of the test SupplierHomePageTest")


    def test_a_cooperator_home_page_statistic(self):
        ''' 供应商客户端给前端  首页-待办事项数据 '''
        test_url = self.base_url + "/cooperator/home_page/statistic/"
        r = requests.get(url=test_url, headers=self._headers, cookies=self._cookies)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn(u"qty_unshipped", r.content)
        self.assertIn(u"qty_wait_pay", r.content)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_b_cooperator_broadcast_list(self):
        ''' 首页-通知列表 '''
        # 查询supplier数据库supplier_broadcast表中共有多少条数据
        sql_1 = "select * from supplier_broadcast"
        num_rows = supplier_mysql_db.DB().get_num_rows(sql_1)

        test_url = self.base_url + "/cooperator/broadcast/list/"
        payload = {'limit': 5, 'offset': 0}
        r = requests.get(url=test_url, params=payload, cookies=self._cookies, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["total"], num_rows)
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_c_cooperator_broadcast_retrieve(self):
        ''' 首页-通知详情 '''
        broadcast_ids = supplier_test_data.query_id("select id from supplier_broadcast", "id")
        broadcast_id = str(broadcast_ids[0])
        test_url = self.base_url + "/cooperator/broadcast/retrieve/" + broadcast_id + "/"
        r = requests.get(url=test_url, cookies=self._cookies, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertEqual(self.result["id"], broadcast_ids[0])
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_d_cooperator_jy_contact_info(self):
        ''' 首页-嘉云对接人信息 '''
        test_url = self.base_url + "/cooperator/jy_contact_info/"
        r = requests.get(url=test_url, cookies=self._cookies, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertNotEqual(self.result['follow_user'], '')
        self.assertNotEqual(self.result['email'], '')
        self.assertNotEqual(self.result['qq_number'], '')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)


    def test_e_cooperator_growth_rating(self):
        ''' 首页-供应商等级信息 '''
        test_url = self.base_url + "/cooperator/growth/rating/"
        r = requests.get(url=test_url, cookies=self._cookies, headers=self._headers)
        self.result = r.json()
        self.assertEqual(r.status_code, requests.codes.ok)
        self.assertIn(u'level', r.content)
        self.assertNotEqual(self.result["supplier_name"], '')
        self.assertLessEqual(r.elapsed.total_seconds(), self.response_time_limit)




if __name__ == '__main__':
    # supplier_test_data.init_data()  # 初始化接口测试数据
    unittest.main()

